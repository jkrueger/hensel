include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

#intervals
bi = RR(68)/RR(10)*x_rb^15 + RR(21)/RR(10)*x_rb^14 + RR(32)/RR(10)*x_rb^13 + 3.0*x_rb^12 + RR(19)/RR(10)*x_rb^11 + RR(92)/RR(10)*x_rb^10 + RR(33)/RR(10)*x_rb^9 + 9.0*x_rb^8 + RR(19)/RR(10)*x_rb^7 + RR(324)/RR(100)*x_rb^6 + RR(41)/RR(10)*x_rb^5 + 4.0*x_rb^4 + RR(29)/RR(10)*x_rb^3 + RR(14)/RR(10)*x_rb^2 + RR(43)/RR(10)*x_rb + 1
db = degree(bi)

qi = newtoninversion(bi,db+1)
norm1error(qi)

#a posteriori validation
br = 6.8*x^15 + 2.1*x^14 + 3.2*x^13 + 3.0*x^12 + 1.9*x^11 + 9.2*x^10 + 3.3*x^9 + 9.0*x^8 + 1.9*x^7 + 3.24*x^6 + 4.1*x^5 + 4.0*x^4 + 2.9*x^3 + 1.4*x^2 + 4.3*x + 1
qr = newtoninversion(br, db+1)
(r,t) = validate_a_posteriori_inverse(qr,bi) 
qr*br
norm1error_rad_mid(r,qr)

#exact result
bq = 68//10*x_q^15 + 21//10*x_q^14 + 32//10*x_q^13 + 30//10*x_q^12 + 19//10*x_q^11 + 92//10*x_q^10 + 33//10*x_q^9 + 9*x_q^8 + 19//10*x_q^7 + 324//100*x_q^6 + 41//10*x_q^5 + 4*x_q^4 + 29//10*x_q^3 + 14//10*x_q^2 + 43//10*x_q + 1
qq = newtoninversion(bq,db+1)
norm1error(qr,qq)