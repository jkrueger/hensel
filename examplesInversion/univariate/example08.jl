include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

#intervals
bi = rev((x_rb^2 + x_rb -1)^3)
db = degree(bi)

qi = newtoninversion(bi,db+1)
norm1error(qi)

#a posteriori validation
br = rev((x^2 + x - 1)^3)
qr = newtoninversion(br, db+1)
(r,t) = validate_a_posteriori_inverse(qr,bi) 
qr*br
norm1error_rad_mid(r,qr)

#exact solution
bq = rev((x_q^2 + x_q - 1)^3)
qq = newtoninversion(bq,db+1)
norm1error(qr,qq)