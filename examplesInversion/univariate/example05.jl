include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

#intervals
bi = x_rb^85 + 17.0*x_rb^80 + RR(136)/RR(10)*x_rb^75 + 68*x_rb^70 + RR(238)/RR(10)*x_rb^65 + RR(6188)/RR(100)*x_rb^60 + RR(12376)/RR(1000)*x_rb^55 + RR(19448)/RR(1000)*x_rb^50 + RR(24310)/RR(1000)*x_rb^45 + RR(2431)/RR(100)*x_rb^40 + RR(19448)/RR(1000)*x_rb^35 + RR(12376)/RR(1000)*x_rb^30 + RR(6188)/RR(100)*x_rb^25 + RR(238)/RR(10)*x_rb^20 + 68*x_rb^15 + RR(136)/RR(10)*x_rb^10 + 17.0*x_rb^5 + 1
db = degree(bi)

qi = newtoninversion(bi,db+1)
norm1error(qi)

#a posteriori validation
br = x^85 + 17.0*x^80 + 13.6*x^75 + 68*x^70 + 23.8*x^65 + 61.88*x^60 + 12.376*x^55 + 19.448*x^50 + 24.31*x^45 + 24.31*x^40 + 19.448*x^35 + 12.376*x^30 + 61.88*x^25 + 23.8*x^20 + 68*x^15 + 13.6*x^10 + 17.0*x^5 + 1
qr = newtoninversion(br, db+1)
(r,t) = validate_a_posteriori_inverse(qr,bi,tmax = 50) 
mullow(qr,br, db+1)
norm1error_rad_mid(r,qr)

#exact
bq = x_q^85 + 17*x_q^80 + 136//10*x_q^75 + 68*x_q^70 + 238//10*x_q^65 + 6188//100*x_q^60 + 12376//1000*x_q^55 + 19448//1000*x_q^50 + 2431//100*x_q^45 + 2431//100*x_q^40 + 19448//1000*x_q^35 + 12376//1000*x_q^30 + 6188//100*x_q^25 + 238//10*x_q^20 + 68*x_q^15 + 136//10*x_q^10 + 17*x_q^5 + 1
qq = newtoninversion(bq,db+1)

mullow(qq,bq,db+1)
norm1error(qr,qq)
