include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

#rational polynomial
K=20
bq =  rev((y_q^30-1)*(y_q^30+2)+(x_q^2+39//10*x_q)^K + 13//10*x_q)
db = degree(bq)
trun = 41

#conversion to interval and real polynomials
bi = convert_polynomial(bq,[RBXY,RBX], RR)
br = convert_polynomial(bq,[RXY,RX], RDF)

#inversion and truncation in x
qq = newtoninversion(bq,db+1)
qi = newtoninversion(bi,db+1)
qr = newtoninversion(br,db+1)
BivPolMod!(qq,trun)
BivPolMod!(qi,trun)
BivPolMod!(qr,trun)

#error measurements and validation
norm1error(qi)
norm1error(qr,qq)
(r,t,s) = validate_a_posteriori_inverse_bivariate(qr,bi,trun)
norm1error_rad_mid(r,qr)
