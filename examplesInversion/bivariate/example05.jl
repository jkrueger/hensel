include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

#rational polynomial
b1q = -22*x_q^6 + 1//12*x_q^5- 2*x_q^4 - 3//39*x_q^3 + 3//2*x_q +2//7
b2q = 1//2*x_q^8 - 1//5*x_q^7 -1//2*x_q^6 + 1//2*x_q^5+2*x_q^4+3//2*x_q^3 +1//2
b3q = x_q^6 +3*x_q^5+1//3*x_q^3+x_q-2
b4q = -3*x_q^8 -1//2*x_q^7 -x_q^6-1//3*x_q^4 + 11//4*x_q^3 + 2//21*x_q^2 -x_q + 1//6
b5q = 1//2*x_q^7-x_q^5-5*x_q^4-x_q^3 -1//4*x_q^2-8*x_q+16
bq = 1 + b1q*y_q + b2q*y_q^2 + b3q*y_q^3 + b4q*y_q^4 + b5q*y_q^5
db = degree(bq)
trun = 9

#conversion to interval and real polynomials
bi = convert_polynomial(bq,[RBXY,RBX], RR)
br = convert_polynomial(bq,[RXY,RX], RDF)

#inversion and truncation in x
qq = newtoninversion(bq,db+1)
qi = newtoninversion(bi,db+1)
qr = newtoninversion(br,db+1)
BivPolMod!(qq,trun)
BivPolMod!(qi,trun)
BivPolMod!(qr,trun)

#error measurements and validation
norm1error(qi)
norm1error(qr,qq)
(r,t,s) = validate_a_posteriori_inverse_bivariate(qr,bi,trun)
norm1error_rad_mid(r,qr)