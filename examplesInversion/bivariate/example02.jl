include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################


#rational polynomial 
trun = 3
bq =  (5//3*x_q + 15//10)*y_q^2 -(7*x_q^2 + 7//5*x_q + 13//10)*y_q + 1
db = degree(bq)
qq = newtoninversion(bq, db+1)
BivPolMod!(qq,trun)

#convert to interval and real polynomials
bi = convert_polynomial(bq,[RBXY,RBX], RR)
br = convert_polynomial(bq,[RXY,RX], RDF)

qi = newtoninversion(bi,db+1)
BivPolMod!(qi,trun)
error_measure1(qi)

qr = newtoninversion(br, db+1)
BivPolMod!(qr,trun)
(r,t) = validate_a_posteriori_inverse_bivariate(qr,bi,3) 
qr*br
norm1error_rad_mid(r,qr)

norm1error(qr,qq)