include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

#exact
N = 14
trun = 337
bq = rev((y_q^2 -1 - x_q^2 + 3*x_q^8 - 5*x_q^24)^N + x_q^N*y_q)
db = degree(bq)
qq = newtoninversion(bq,db+1)
BivPolMod!(qq,trun)
bq*qq
BivPolMod!(mullow(bq,qq,29),trun)
#intervals
bi = convert_polynomial(bq,[RBXY,RBX], RR)
qi = newtoninversion(bi,db+1)
BivPolMod!(qi, trun)
#real coefficients
br = convert_polynomial(bq,[RXY,RX], RDF)
qr = newtoninversion(br,db+1)
BivPolMod!(qr,trun)

#exact error
norm1error(qr,qq)
#interval error
norm1error(qi)
#validation error
(r,t,s) = validate_a_posteriori_inverse_bivariate(qr,bi,trun)
norm1error_rad_mid(r,qr)