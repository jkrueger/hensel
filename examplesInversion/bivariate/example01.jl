include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

#intervals

#rational polynomial
bq = (1//5*x_q^2+3//2*x_q +11//10)*y_q^2 +(3*x_q+75//10)*y_q + 1
db = degree(bq)
qq = newtoninversion(bq, db+1)
BivPolMod!(qq,3)

#conversion to interval polynomial and real polynomial
bi = convert_polynomial(bq,[RBXY,RBX],RR)
br = convert_polynomial(bq,[RXY,RX],RDF)

#inversion of interval polynomial
qi = newtoninversion(bi,db+1)
BivPolMod!(qi,3)
error_measure1(qi)

#inversion of real polynomial and its validation
qr = newtoninversion(br, db+1)
BivPolMod!(qr,3)
(r,t) = validate_a_posteriori_inverse_bivariate(qr,bi,3) 
norm1error_rad_mid(r,qr)

#exact error
norm1error(qr,qq)
