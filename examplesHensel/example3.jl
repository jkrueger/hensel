include("../functions.jl")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#real balls
RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#example 3
N = 14
#real functions
Fr = (y^2 -1 - x^2 + 3*x^8 - 5*x^24)^N + x^N*y
gr3 = (y-1)^N
hr3 = (y+1)^N

#rational functions
Fq = (y_q^2 -1 - x_q^2 + 3*x_q^8 - 5*x_q^24)^N + x_q^N*y_q
gq3 = (y_q-1)^N
hq3 = (y_q+1)^N

R = 0.6319193895

hensel_test(Fr, gr3, hr3, Fq, gq3, hq3, 4, R)


(dr3, sr3, tr3) = gcdx(gr3,hr3)
(dq3, sq3, tq3) = gcdx(gq3, hq3)

@time (grstar3, hrstar3) = henseltruncate(2, Fr, gr3, hr3, sr3, tr3) 
BivPolMod!(Fr-grstar3*hrstar3,2)

@time (grstar3, hrstar3) = henseltruncate(4, Fr, gr3, hr3, sr3, tr3) 
BivPolMod!(Fr-grstar3*hrstar3,4)
(gqstar3, hqstar3) = henseltruncate(4, Fq, gq3, hq3, sq3, tq3)

#plot()
#plot_error_along_R(Fq, grstar3*hrstar3, "gstar*hstar, n = 4")
plot_error_along_R(gqstar3, grstar3, "gstar n = 4", Fq, 0, 2R, R)
plot_error_along_R!(hqstar3, hrstar3, "hstar n = 4", 0, 2R)

@time (grstar3, hrstar3) = henseltruncate(8, Fr, gr3, hr3, sr3, tr3)
BivPolMod!(Fr-grstar3*hrstar3,8)
(gqstar3, hqstar3) = henseltruncate(8,Fq,gq3,hq3,sq3,tq3)
subtract(gqstar3, grstar3)
grstar3
gqstar3

plot_error_along_R(gqstar3, grstar3, "gstar n = 8", Fq, 0, 2R, R)
plot_error_along_R!(hqstar3, hrstar3, "hstar n = 8", 0, 2R)


@time (grstar3, hrstar3) = henseltruncate(16, Fr, gr3, hr3, sr3, tr3) 
BivPolMod!(Fr-grstar3*hrstar3,16)
(gqstar3, hqstar3) = henseltruncate(16, Fq, gq3, hq3, sq3, tq3)
#plot()
#plot_error_along_R!(Fq, grstar3*hrstar3, "grstar*hrstar, n = 16", 0, 0.0001)
plot_error_along_R(gqstar3, grstar3, "gstar n = 16", Fq, 0, 2R, R)
plot_error_along_R!(hqstar3, hrstar3, "hstar n = 16", 0, 2R)

#@time (gstar3, hstar3) = henseltruncate(32, F, g3, h3, s3, t3) 
#BivPolMod!(F-gstar3*hstar3,32)

#@time (gstar3, hstar3) = henseltruncate(128, F, g3, h3, s3, t3)
#BivPolMod!(F-gstar3*hstar3,128)
@time 1+1

hensel_test(Fr, gr3, hr3, Fq, gq3, hq3, 4, R)

###################################
N = 14
Fq = (y_q^2 -1 - x_q^2 + 3*x_q^8 - 5*x_q^24)^N + x_q^N*y_q
gq3 = (y_q-1)^N
hq3 = (y_q+1)^N
BivPolMod!(gq3,1)
BivPolMod!(hq3,1)

gi = convert_polynomial(gq3, [RBXY,RBX], RR)
gr = convert_polynomial(gq3,[RXY,RX],RDF)
hi = convert_polynomial(hq3, [RBXY,RBX], RR)
hr = convert_polynomial(hq3,[RXY,RX],RDF)
fi = convert_polynomial(Fq, [RBXY,RBX], RR)
fr = convert_polynomial(Fq,[RXY,RX],RDF)

deg = 32
@time (gqstar, hqstar) = henseltruncate(deg,Fq,gq3,hq3)
@time (gistar, histar) = henseltruncate(deg,fi,gi,hi)
@time (gvstar, hvstar) = henseltruncate(deg,fi,gi,hi,validation = true)
#@time (grstar, hrstar) = henseltruncate(128,fr,gr,hr)
@time (grstar, hrstar) = henseltruncate(deg,fr,gr,hr)
@time (grv,hrv) = validate_Hensel_a_posteriori(deg,fi,gi,hi,tmax = 150);

norm1error(grstar,gqstar)
norm1error(hrstar,hqstar)
norm1error(gistar)
norm1error(histar)
norm1error(gvstar)
norm1error(hvstar)
norm1error(grv)
norm1error(hrv)

range_of_coefficients(Fq)

error_measure1(gqstar,grstar, R/2)
error_measure1(hqstar,hrstar, R/2)
error_measure1(gistar, R/2)
error_measure1(histar, R/2)
error_measure1(gvstar, R/2)
error_measure1(hvstar, R/2)
error_measure1(grv, R/2)
error_measure1(hrv, R/2)