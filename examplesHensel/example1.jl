include("../functions.jl")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#real balls
RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

############################
##beginning of the example##
############################

#real functions
gr = y -(2*x^3 -5*x^2 + 8*x -5)
hr = y - (x^3 -3* x^2 + 3*x -1)
gr*hr
fr = y^2 + 6*y +5 -10*x

#rational functions for error comparison
gq = y_q -(2*x_q^3 -5*x_q^2 + 8*x_q -5)
hq = y_q - (x_q^3 -3* x_q^2 + 3*x_q -1)
fq = y_q^2 + 6*y_q +5 -10*x_q

R = 0.3999999999068677425384521484375

BivPolMod!(gr,1)
BivPolMod!(hr,1)
(d, sr, tr) = gcdx(gr, hr)

BivPolMod!(gq,1)
BivPolMod!(hq,1)
(d, sq, tq) = gcdx(gq,hq)

@time (grstar, hrstar) = henseltruncate(2, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar*hrstar,2)

@time (grstar, hrstar) = henseltruncate(4, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar*hrstar,4)

@time (grstar, hrstar) = henseltruncate(8, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar*hrstar,8)

@time (grstar, hrstar) = henseltruncate(16, fr, gr, hr, sr, tr)
fr - grstar*hrstar
BivPolMod!(fr-grstar*hrstar,16)

@time (gqstar, hqstar) = henseltruncate(16, fq, gq, hq, sq, tq)
#error_with_R(fq, grstar*hrstar, fq)
plot_error_along_R(fq,grstar*hrstar, "grstar*hrstar", fq, 0,0.5, R)


@time (grstar, hrstar) = henseltruncate(128, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar*hrstar,128)
#error_with_R(fq, grstar*hrstar, fq)
plot_error_along_R(fq,grstar*hrstar, "grstar*hrstar", fq, 0, 0.42, R)

hensel_test(fr, gr, hr, fq, gq, hq, 7, R)

####################################
#hensel in intervals and validation#
####################################
R = 0.3999999999068677425384521484375
gq = y_q -(2*x_q^3 -5*x_q^2 + 8*x_q -5)
hq = y_q - (x_q^3 -3* x_q^2 + 3*x_q -1)
fq = y_q^2 + 6*y_q +5 -10*x_q
BivPolMod!(gq,1)
BivPolMod!(hq,1)

gi = convert_polynomial(gq, [RBXY,RBX], RR)
gr = convert_polynomial(gq,[RXY,RX],RDF)
hi = convert_polynomial(hq, [RBXY,RBX], RR)
hr = convert_polynomial(hq,[RXY,RX],RDF)
fi = convert_polynomial(fq, [RBXY,RBX], RR)
fr = convert_polynomial(fq,[RXY,RX],RDF)

@time (gqstar, hqstar) = henseltruncate(128,fq,gq,hq)
@time (gistar, histar) = henseltruncate(128,fi,gi,hi)
@time (gvstar, hvstar) = henseltruncate(128,fi,gi,hi,validation = true)
@time (grstar, hrstar) = henseltruncate(128,fr,gr,hr)
@time (grv,hrv) = validate_Hensel_a_posteriori(128,fi,gi,hi);

norm1error(grstar,gqstar)
norm1error(hrstar,hqstar)
norm1error(gistar)
norm1error(histar)
norm1error(gvstar)
norm1error(hvstar)
norm1error(grv)
norm1error(hrv)

#=error_measure1(gqstar,grstar, R/2)
error_measure1(hqstar,hrstar, R/2)
error_measure1(gistar, R/2)
error_measure1(histar, R/2)
error_measure1(gvstar, R/2)
error_measure1(hvstar, R/2)
error_measure1(grv, R/2)
error_measure1(hrv, R/2)=#
