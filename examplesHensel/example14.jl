include("../functions.jl")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

K=120

fr = (y^130-1)*(y^130+2)+(x^2+4*x)^K + x
gr = y^130-1
hr = y^130+2

fq = (y_q^130-1)*(y_q^130+2)+(x_q^2+4*x_q)^K + x_q
gq = y_q^130 -1
hq = y_q^130 +2

#R = 0.2439395990

hensel_test(fr,gr,hr,fq,gq,hq,7, 2)
