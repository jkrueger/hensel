include("../functions.jl")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#real balls
RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#example 4
N = 14
#real functions
gr = (y - (x^2+2*x +5//1)^N)
hr = (y - (2*x^2 -7*x +1//1)^N)
fr =  y^2 - (5^N+1)*y + (x^34 -x + 1)^N*(x^13 -27*x+5//1)^N
BivPolMod!(fr-gr*hr,1)

#rational functions
gq = (y_q - (x_q^2+2*x_q +5//1)^N)
hq = (y_q - (2*x_q^2 -7*x_q +1//1)^N)
fq =  y_q^2 - (5^N+1)*y_q + (x_q^34 -x_q + 1)^N*(x_q^13 -27*x_q+5//1)^N


R = 0.4097119004


BivPolMod!(gr,1)
BivPolMod!(hr,1)
BivPolMod!(gq,1)
BivPolMod!(hq,1)
(dr,sr,tr) = gcdx(gr,hr)
(dq, sq, tq) = gcdx(gq, hq)


@time (grstar4, hrstar4) = henseltruncate(2, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar4*hrstar4,2)

@time (grstar4, hrstar4) = henseltruncate(4,fr,gr,hr,sr,tr)
BivPolMod!(fr-grstar4*hrstar4,4)

@time (grstar4, hrstar4) = henseltruncate(8, fr, gr, hr, sr, tr)
(gqstar4, hqstar4) = henseltruncate(8, fq, gq, hq, sq, tq)
BivPolMod!(fr-grstar4*hrstar4,8)
plot_error_along_R(fq, grstar4*hrstar4, fq, "gstar*hstar to f, n = 8", 0, 1, R)
plot_error_along_R!(gqstar4*hqstar4, grstar4*hrstar4, "gstar*hstar, n = 8", 0,1)
#error_with_R(fq,grstar4*hrstar4,fq)

@time (grstar4, hrstar4) = henseltruncate(16, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar4*hrstar4,16)

@time (grstar4, hrstar4) = henseltruncate(32, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar4*hrstar4,32)

@time (grstar4, hrstar4) = henseltruncate(64, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar4*hrstar4,64)

@time (grstar4, hrstar4) = henseltruncate(128, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar4*hrstar4,128)

#error_with_R(gq,grstar4,fq)
hensel_test(fr, gr, hr, fq, gq, hq, 7, R)

####################################
N = 14
gq = (y_q - (x_q^2+2*x_q +5//1)^N)
hq = (y_q - (2*x_q^2 -7*x_q +1//1)^N)
fq =  y_q^2 - (5^N+1)*y_q + (x_q^34 -x_q + 1)^N*(x_q^13 -27*x_q+5//1)^N
BivPolMod!(gq,1)
BivPolMod!(hq,1)

gi = convert_polynomial(gq, [RBXY,RBX], RR)
gr = convert_polynomial(gq,[RXY,RX],RDF)
hi = convert_polynomial(hq, [RBXY,RBX], RR)
hr = convert_polynomial(hq,[RXY,RX],RDF)
fi = convert_polynomial(fq, [RBXY,RBX], RR)
fr = convert_polynomial(fq,[RXY,RX],RDF)

deg = 128
@time (gqstar, hqstar) = henseltruncate(deg,fq,gq,hq)
@time (gistar, histar) = henseltruncate(deg,fi,gi,hi)
@time (gvstar, hvstar) = henseltruncate(deg,fi,gi,hi,validation = true)
@time (grstar, hrstar) = henseltruncate(deg,fr,gr,hr)
@time (grv, hrv) = validate_Hensel_a_posteriori(deg,fi,gi,hi,tmax = 50);

norm1error(grstar,gqstar)
norm1error(hrstar,hqstar)
norm1error(gistar)
norm1error(histar)
norm1error(gvstar)
norm1error(hvstar)
norm1error(grv)
norm1error(hrv)

range_of_coefficients(fq)
range_of_coefficients(gq)
range_of_coefficients(hq)

error_measure1(gqstar,grstar, R/2)
error_measure1(hqstar,hrstar, R/2)
error_measure1(gistar, R/2)
error_measure1(histar, R/2)
error_measure1(gvstar, R/2)
error_measure1(hvstar, R/2)