Tous les erreurs sont mésurés en tant que (sum(|radius(q_i)|))/(sum(|midpoint(q_i)|)) ou ||r||_1/||q||_1

Numerical error
Interval Arithmetic
A posteriori validation Newton Inversion
A posteriori validation Hensel

Example 1:
g = y - 2.0*x^3 + 5.0*x^2 - 8.0*x + 5.0
h = y - 1.0*x^3 + 3.0*x^2 - 3.0*x + 1
f = y^2 + 6.0*y - 10.0*x + 5.0

deg = 128
g:
Numerical error
3.073047744299522073702879821501952064316033904235377398217144780554645461701889e-16
Interval Arithmetic
3.295840702537403e-13
A posteriori validation Newton Inversion
5.1434656222423e-13
A posteriori validation Hensel
9.230572374543876e-16

h:
Numerical error
3.073047744299522073702879821501952064316033904288944799482866546544934645878239e-16
Interval Arithmetic
2.2487520973391352e-13
A posteriori validation Newton Inversion
3.4455103601078275e-13
A posteriori validation Hensel
9.23057236181133e-16

Example 3:

N = 14
F = (y^2 -1 - x^2 + 3*x^8 - 5*x^24)^N + x^N*y
g = (y-1)^N
h = (y+1)^N
deg_y(f) = 28, deg_x(f) = 336
deg_y(g) = 14
deg_y(h) = 14
coeffs(F) [1, 4145547656250]
coeffs(g) [1, 3432]
coeffs(h) [1, 3432]

deg = 128
g:
Numerical error
1.799071642918580039146673035882710605696778861742944652536654754831160036041804e+123
Interval Arithmetic
NaN
A posteriori validation Newton Inversion
NaN
A posteriori validation Hensel
5.048357168552956e-15

h:
Numerical error
3.919080082664558129193009256076842606463647933069713902492901090508697341246231e+122
Interval Arithmetic
NaN
A posteriori validation Newton Inversion
NaN
A posteriori validation Hensel
3.3478948552608092e-15

Example 4:

N = 14
g = (y - (x^2+2*x +5)^N)
h = (y - (2*x^2 -7*x +1)^N)
f =  y^2 - (5^N+1)*y + (x^34 -x + 1)^N*(x^13 -27*x+5)^N

deg_y(f) = 2, deg_x(f) = 658
deg_y(g) = 1
deg_y(h) = 1
coeffs(f) [14, 230180635092391873604047333]
coeffs(g) [6103515625]
coeffs(h) [1]

deg_x = 128
g:
Numerical error
1.293739898291606770858891793699838129282023560849420700437389486878454288200745e-15
Interval Arithmetic
2.758557443036354e-11
A posteriori validation Newton Inversion
3.1702512370277e-11
A posteriori validation Hensel
NaN (convergence after 21 steps)

h:
Numerical error
1.293739898291606770858891793699838129282023560969537462060804378901182659050028e-15
Interval Arithmetic
1.6024956315588576e-11
A posteriori validation Newton Inversion
1.8210579515364143e-11
A posteriori validation Hensel
NaN (convergence after 21 steps)

Example 5:

f = (6.0*x^3 + 6.0*x^2 - 3.0)*y^2 + (3.0*x^2 + x)*y + 3.0*x^3 + 6.0*x^2 + 3.0
g = -3.0*y + 3.0
h = y + 1

deg_y(f) = 2, deg_x(f) = 3
deg_y(g) = 1
deg_y(h) = 1
coeffs(f) [3,6]

deg_x = 128

g:
Numerical error
1.334219302660418279091624555201719942013031537053451681132887487172422628433282e-15
Interval Arithmetic
1.9930654211464116e-6
A posteriori validation Newton Inversion
NaN
A posteriori validation Hensel
5.1635540851203456e-14

h:
Numerical error
2.16849989073389847585954346440662157539204235598712999324066093119070539311752e-16
Interval Arithmetic
1.7266373126064373e-7
A posteriori validation Newton Inversion
NaN
A posteriori validation Hensel
5.63055229026863e-15

Example 6:
f = (y- (x^24 + x^18 -x-1)^N)*(y- (x^2 + x + 7))
g = y- 7 
h = y - 1

deg_y(f) = 2, deg_x(f) = 336
deg_y(g) = 1
deg_y(h) = 1
coeffs(f) [7, 33993960]

deg_x = 128

g:
Numerical error
4.807093711891145280897901198791667651611210548554026688158420275044685176952527e+86
Interval Arithmetic
NaN
A posteriori validation Newton Inversion
NaN
A posteriori validation Hensel
NaN

h:
Numerical error
2.457314018718598293500699001440523357460158888660413042455230935396546355616896e+79
Interval Arithmetic
NaN
A posteriori validation Newton Inversion
NaN
A posteriori validation Hensel
NaN

Example 7:
K = 20
f = (3*y - (x-2)^K)*(y -(x^2+4)^K) 
g = 3*y - 2^K 
h = y - 4^K

deg_y(f) = 2, deg_x(f) = 40
deg_y(g) = 1
deg_y(h) = 1
coeffs(f) [3, 31779833729274766950400]

deg_x = 128

g:
Numerical error
1.212867019327621370365705621322108580181888001199160610359510995280116791340155e-12
Interval Arithmetic
A posteriori validation Newton Inversion
NaN
A posteriori validation Hensel
no convergence

h:
Numerical error
2.303320472637815587945409114009395035258543429258678576117527183605307279344562e-16
Interval Arithmetic
A posteriori validation Newton Inversion
NaN
A posteriori validation Hensel
no convergence 

Example 8:
K = 20
f = (3*y - (x-2)^K)*(y -(x^2+4)^K) 
g = 3*y - 2^K 
h = y - 4^K

deg_y(f) = 2, deg_x(f) = 40
deg_y(g) = 1
deg_y(h) = 1
coeffs(f) [3, 31779833729274766950400]

deg_x = 128

g:
Numerical error
1.212867019327621370365705621322108580181888001199160610359510995280116791340155e-12
Interval Arithmetic
NaN 
A posteriori validation Newton Inversion
NaN 
A posteriori validation Hensel
no convergence

h:
Numerical error
2.303320472637815587945409114009395035258543429258678576117527183605307279344562e-16
Interval Arithmetic
NaN 
A posteriori validation Newton Inversion
NaN 
A posteriori validation Hensel
no convergence

deg_x = 32

g:
Numerical error
2.954156279882755885534388313821472373203320568330448444913071879964081844588337e-12
Interval Arithmetic
0.0001336930947672954
A posteriori validation Newton Inversion
NaN 
A posteriori validation Hensel
NaN (convergence after 34 steps)

h:
Numerical error
3.074450628105609323953273371480016225948436639150783114230151223183089598631157e-13
Interval Arithmetic
8.550969948828424e-5
A posteriori validation Newton Inversion
NaN 
A posteriori validation Hensel
NaN (convergence after 34 steps)

Example 9:
f = (y^5-1)*(y^5+2)+x
g = y-1
h = y+2

deg_y(f) = 10, deg_x(f) = 1
deg_y(g) = 5
deg_y(h) = 5
coeffs(f) [2]

deg_x = 128

g:
Numerical error
9.277233482758887051048231441936821383460365565665805462494700731717860632586342e-18
Interval Arithmetic
6.43145290051548e-12
A posteriori validation Newton Inversion
8.170211376193516e-12
A posteriori validation Hensel
1.354745879692541e-16

h:
Numerical error
6.534085428668992890198018740684461524817687133298758453874876334528435509155081e-18
Interval Arithmetic
3.523310905918902e-12
A posteriori validation Newton Inversion
4.475617818678794e-12
A posteriori validation Hensel
9.541664903119242e-17

Example 10:

f = (y^5-1)*(y^5+2)+(x^2+4*x)^K 
g = y^5-1
h = y^5+2

deg_y(f) = 10, deg_x(f) = 40
deg_y(g) = 5
deg_y(h) = 5
coeffs(f) [2, 20809116549120]

deg_x = 128

g:
Numerical error
6.890723389454847456140779824534644648874718212210294630444264458516851483694859e-16
Interval Arithmetic
5.65193208594629e-11
A posteriori validation Newton Inversion
1.0072839017470755e-10
A posteriori validation Hensel
4.126270913498168e-15

h:
Numerical error
6.890723389454847456140779824534644648874718212210294630444264458516851483691408e-16
Interval Arithmetic
4.928445667544449e-11
A posteriori validation Newton Inversion
8.774135942571211e-11
A posteriori validation Hensel
4.126270913498168e-15

Example 11:
f = (y^15-1)*(y^15+2)+x
g = y^15-1
h = y^15+2

deg_y(f) = 30, deg_x(f) = 1
deg_y(g) = 15
deg_y(h) = 15
coeffs(f) [2]

deg_x = 128

g:
Numerical error
9.277233482758887051048231441936821383460365565665805462494700731717860632586342e-18
Interval Arithmetic
6.431452978747961e-12
A posteriori validation Newton Inversion
8.170212487666917e-12
A posteriori validation Hensel
1.354745879692541e-16

h:
Numerical error
6.534085428668992890198018740684461524817687133298758453874876334528435509155081e-18
Interval Arithmetic
3.523310905918902e-12
A posteriori validation Newton Inversion
4.4756182934382825e-12
A posteriori validation Hensel
9.541664903119242e-17


Example 12: 
f = (y^15-1)*(y^15+2)+(x^2+4*x)^K
g = y^15-1
h = y^15+2

deg_y(f) = 30, deg_x(f) = 40
deg_y(g) = 15
deg_y(h) = 15
coeffs(f) [2, 20809116549120]

g:
Numerical error
6.890723389454847456140779824534644648874718212210294630444264458516851483694859e-16
Interval Arithmetic
5.6519321264720244e-11
A posteriori validation Newton Inversion
1.0097760021402075e-10
A posteriori validation Hensel
4.126270913498168e-15

h:
Numerical error
6.890723389454847456140779824534644648874718212210294630444264458516851483691408e-16
Interval Arithmetic
4.928445667544449e-11
A posteriori validation Newton Inversion
8.802274209000801e-11
A posteriori validation Hensel
4.126270913498168e-15

Example 13: 
f = (y^30-1)*(y^30+2)+(x^2+4*x)^K + x
g = y^30-1
h = y^30+2

deg_y(f) = 60, deg_x(f) = 40
deg_y(g) = 30
deg_y(h) = 30
coeffs(f) [2, 20809116549120]

deg_x = 128

g:
Numerical error
9.926404348498099331238677813125519317359014645219632318620521274277287107469128e-16
Interval Arithmetic
7.714649465066166e-11
A posteriori validation Newton Inversion
8.677404667193175e-11
A posteriori validation Hensel
4.998273516465546e-15

h:
Numerical error
9.926404348498099331238677813125519317359014645219632318620521274277287107466367e-16
Interval Arithmetic
6.743562412716703e-11
A posteriori validation Newton Inversion
7.582577036181246e-11
A posteriori validation Hensel
4.998273516465546e-15

