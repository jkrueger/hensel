include("../functions.jl")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#real balls
RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#############
##example 5##
#############

#real functions
fr0 = 1 +2*x^2 +x^3
fr1 = x + 3* x^2
fr2 = -1 +2*x^2 + 2*x^3
fr = (3*fr0) + fr1*y + 3*fr2*y^2

gr = 3*(1//1 -y)
hr = y+1

#rational functions
fq0 = 1 +2*x_q^2 +x_q^3
fq1 = x_q + 3* x_q^2
fq2 = -1 +2*x_q^2 + 2*x_q^3
fq = (3*fq0) + fq1*y_q + 3*fq2*y_q^2

gq = 3*(1-y_q)
hq = y_q +1

R = 0.5738794244825839996337890625


BivPolMod!(fr-gr*hr,1)

(d, sr, tr) = gcdx(gr,hr)
(d, sq, tq) = gcdx(gq,hq)

@time (grstar, hrstar) = henseltruncate(128, fr, gr, hr, sr, tr)

BivPolMod!(fr-grstar*hrstar,128)

@time (gqstar, hqstar) = henseltruncate(128, fq, gq, hq, sq, tq)

#(e,R) = error_with_R(fq, grstar*hrstar, fq)
plot_error_along_R(fq, grstar*hrstar, "gstar * hstar to f", fq, 0, 0.575, R)
#(e,R) = error_with_R(gq, grstar, fq)
plot_error_along_R!(gq, grstar, "gstar",0,0.575)
#(e,R) = error_with_R(hq, hrstar, fq)
plot_error_along_R!(hq, hrstar, "hstar", 0, 0.575)

hensel_test(fr, gr, hr, fq, gq, hq, 7, R)

########################################
fq0 = 1 +2*x_q^2 +x_q^3
fq1 = x_q + 3* x_q^2
fq2 = -1 +2*x_q^2 + 2*x_q^3
fq = (3*fq0) + fq1*y_q + 3*fq2*y_q^2

gq = 3*(1-y_q)
hq = y_q +1

BivPolMod!(gq,1)
BivPolMod!(hq,1)

gi = convert_polynomial(gq, [RBXY,RBX], RR)
gr = convert_polynomial(gq,[RXY,RX],RDF)
hi = convert_polynomial(hq, [RBXY,RBX], RR)
hr = convert_polynomial(hq,[RXY,RX],RDF)
fi = convert_polynomial(fq, [RBXY,RBX], RR)
fr = convert_polynomial(fq,[RXY,RX],RDF)

deg = 128
@time (gqstar, hqstar) = henseltruncate(deg,fq,gq,hq)
@time (gistar, histar) = henseltruncate(deg,fi,gi,hi)
@time (gvstar, hvstar) = henseltruncate(deg,fi,gi,hi,validation = true)
@time (grstar, hrstar) = henseltruncate(deg,fr,gr,hr)
@time (grv,hrv) = validate_Hensel_a_posteriori(deg,fi,gi,hi);

norm1error(grstar,gqstar)
norm1error(hrstar,hqstar)
norm1error(gistar)
norm1error(histar)
norm1error(gvstar)
norm1error(hvstar)
norm1error(grv)
norm1error(hrv)

range_of_coefficients(fq)
range_of_coefficients(gq)
range_of_coefficients(hq)

error_measure1(gqstar,grstar, R/2)
error_measure1(hqstar,hrstar, R/2)
error_measure1(gistar, R/2)
error_measure1(histar, R/2)
error_measure1(gvstar, R/2)
error_measure1(hvstar, R/2)
error_measure1(grv, R/2)
error_measure1(hrv, R/2)