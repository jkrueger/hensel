include("../functions.jl")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#real balls
RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

K = 20
fr = (3*y - (x-2)^K)*(y -(x^2+4)^K) 
gr = 3*y - 2^K 
hr = y - 4^K

fq = (3*y_q - (x_q-2)^K)*(y_q -(x_q^2+4)^K)
gq = 3*y_q - 2^K 
hq = y_q - 4^K

R = 1.311590266

BivPolMod!(fr-gr*hr,1)

(d,sr,tr) = gcdx(gr,hr)

 
@time hensel_test(fr,gr,hr,fq,gq, hq, 7, R)
#(e, R) = error_with_R(fq,gr*hr,fq)
scatter!([R],[0], label = "R")
@time (grstar7,hrstar7) = henseltruncate(128, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar7*hrstar7, 128)

#########################################
K = 20

fq = (3*y_q - (x_q-2)^K)*(y_q -(x_q^2+4)^K)
gq = 3*y_q - 2^K 
hq = y_q - 4^K

BivPolMod!(gq,1)
BivPolMod!(hq,1)

gi = convert_polynomial(gq, [RBXY,RBX], RR)
gr = convert_polynomial(gq,[RXY,RX],RDF)
hi = convert_polynomial(hq, [RBXY,RBX], RR)
hr = convert_polynomial(hq,[RXY,RX],RDF)
fi = convert_polynomial(fq, [RBXY,RBX], RR)
fr = convert_polynomial(fq,[RXY,RX],RDF)

deg = 128
@time (gqstar, hqstar) = henseltruncate(deg,fq,gq,hq)
@time (gistar, histar) = henseltruncate(deg,fi,gi,hi)
@time (gvstar, hvstar) = henseltruncate(deg,fi,gi,hi,validation = true)
@time (grstar, hrstar) = henseltruncate(deg,fr,gr,hr)
@time (grv,hrv) = validate_Hensel_a_posteriori(deg,fi,gi,hi,tmax = 100);

norm1error(grstar,gqstar)
norm1error(hrstar,hqstar)
norm1error(gistar)
norm1error(histar)
norm1error(gvstar)
norm1error(hvstar)
norm1error(grv)
norm1error(hrv)

range_of_coefficients(fq)

error_measure1(gqstar,grstar, R/2)
error_measure1(hqstar,hrstar, R/2)
error_measure1(gistar, R/2)
error_measure1(histar, R/2)
error_measure1(gvstar, R/2)
error_measure1(hvstar, R/2)
error_measure1(grv, R/2)
error_measure1(hrv, R/2)