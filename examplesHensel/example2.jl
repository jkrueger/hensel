include("../functions.jl")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#example 1 slightly to the left (example 2)

#real functions
gr = y -(2*x^3 -5*x^2 + 8*x -5)
hr = y - (x^3 -3* x^2 + 3*x -1)
fr = y^2 + 6*y +5 -10*x

#rational functions
gr2 = y -(2*x^3 -5*x^2 + (8-1e-6)*x -5)
hr2= y - (x^3 -3.1* x^2 + 3*x -1.1) #y - (x^3 -(3+1e-9)* x^2 + 3*x -1)

BivPolMod!(gr2,1)
BivPolMod!(hr2,1)
(d,sr2,tr2) = gcdx(gr2,hr2)

@time (grstar2, hrstar2) = henseltruncate(1, fr, gr2, hr2, sr2, tr2)
BivPolMod!(fr-grstar2*hrstar2,1)

@time (grstar2, hrstar2) = henseltruncate(2, fr, gr2, hr2, sr2, tr2)
BivPolMod!(fr-grstar2*hrstar2,2)

@time (grstar2, hrstar2) = henseltruncate(4, fr, gr2, hr2, sr2, tr2)
BivPolMod!(fr-grstar2*hrstar2,4)

@time (grstar2, hrstar2) = henseltruncate(8, fr, gr2, hr2, sr2, tr2)
BivPolMod!(fr-grstar2*hrstar2,8)

@time (grstar2, hrstar2) = henseltruncate(16, fr, gr2, hr2, sr2, tr2)
BivPolMod!(fr-grstar2*hrstar2,16)

@time (grstar2, hrstar2) = henseltruncate(128, fr, gr2, hr2, sr2, tr2)
BivPolMod!(fr-grstar2*hrstar2,128)

h22 = (1-1e-4)*y - (x^3 -3* x^2 + 3*x -1)
h22prime = deepcopy(h22)
BivPolMod!(h22prime,1)
(d,s22, t22) = gcdx(gr2, h22prime)
@time (gstar22, hstar22) = henseltruncate(8,fr, gr2, h22, s22, t22)
BivPolMod!(fr-gstar22*hstar22,8)
