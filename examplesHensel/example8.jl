include("../functions.jl")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#real balls
RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")


#Example 8:
K = 20
#real
fr = (3*y - (x-2)^K)*(y -(x^2+4)^K) 
gr = 3*y - 2^K 
hr = y - 4^K

#rational
fq = (3*y_q - (x_q-2)^K)*(y_q -(x_q^2+4)^K) 
gq = 3*y_q - 2^K 
hq = y_q - 4^K

R = 1.311590266245814591947009553063452358401264064013957977294921875


(d,sq,tq) = gcdx(gq,hq)
(d,sr,tr) = gcdx(gr,hr)

@time (gstarq,hstarq) = henseltruncate(128, fq, gq, hq, sq, tq)
@time (gstarr,hstarr) = henseltruncate(128, fr, gr, hr, sr, tr)


frprime = gstarr*hstarr
fqprime = gstarq*hstarq
error_measure1(gstarq,gstarr, R/2)
error_measure1(hstarq,hstarr, R/2)
error_measure1(fqprime,frprime, R/2)
error_measure1(fq, frprime, R/2)    

#error_with_R(gstarq,gstarr,fq)
#error_with_R(hstarq,hstarr,fq)
#error_with_R(fq, gstarr*hstarr,fq)

range(0,2,200)
range(0,3,200)


#(e,R) = error_with_R(fq,frprime,fq)
#e = convert(Float64, e)
#R = convert(Float64, R)
error_measure1(fq,frprime,R/2)

error_measure1(fq,frprime, 1/2)

rrange = range(0,1.34,200)
plot(rrange, map(u-> (error_measure1(gstarq,gstarr,u)), rrange), label = "gstar")
scatter!([R],[0], label = "R")

plot!(rrange, map(u-> (error_measure1(fq,frprime,u)), rrange), label = "gstar*hstar to f")

plot!(rrange, map(u-> (error_measure1(hstarq,hstarr,u)), rrange), label = "hstar")
#scatter!([R],[0])

hensel_test(fr, gr, hr, fq, gq, hq, 7, R)

######################################
K = 20
fq = (3*y_q - (x_q-2)^K)*(y_q -(x_q^2+4)^K) 
gq = 3*y_q - 2^K 
hq = y_q - 4^K
BivPolMod!(gq,1)
BivPolMod!(hq,1)

gi = convert_polynomial(gq, [RBXY,RBX], RR)
gr = convert_polynomial(gq,[RXY,RX],RDF)
hi = convert_polynomial(hq, [RBXY,RBX], RR)
hr = convert_polynomial(hq,[RXY,RX],RDF)
fi = convert_polynomial(fq, [RBXY,RBX], RR)
fr = convert_polynomial(fq,[RXY,RX],RDF)

deg = 32#128
@time (gqstar, hqstar) = henseltruncate(deg,fq,gq,hq)
@time (gistar, histar) = henseltruncate(deg,fi,gi,hi)
@time (gvstar, hvstar) = henseltruncate(deg,fi,gi,hi,validation = true)
@time (grstar, hrstar) = henseltruncate(deg,fr,gr,hr)
@time (grv, hrv) = validate_Hensel_a_posteriori(deg,fi,gi,hi, tmax = 50);

norm1error(grstar,gqstar)
norm1error(hrstar,hqstar)
norm1error(gistar)
norm1error(histar)
norm1error(gvstar)
norm1error(hvstar)
norm1error(grv)
norm1error(hrv)

range_of_coefficients(fq)

error_measure1(gqstar,grstar, R/2)
error_measure1(hqstar,hrstar, R/2)
error_measure1(gistar, R/2)
error_measure1(histar, R/2)
error_measure1(gvstar, R/2)
error_measure1(hvstar, R/2)
error_measure1(grv, R/2)
error_measure1(hrv, R/2)