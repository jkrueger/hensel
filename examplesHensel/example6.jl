include("../functions.jl")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#real balls
RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#############
##example 6##
#############
N = 14
#real functions
fr = (y- (x^24 + x^18 -x-1)^N)*(y- (x^2 + x + 7))
gr = y- 7 
hr = y - 1

#rational functions
fq = (y_q- (x_q^24 + x_q^18 -x_q-1)^N)*(y_q- (x_q^2 + x_q + 7))
gq = y_q- 7 
hq = y_q - 1

R = 0.151133528687885

BivPolMod!(fr-gr*hr,1)
(d, sr, tr) = gcdx(gr,hr)

@time (grstar6, hrstar6) = henseltruncate(2, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar6*hrstar6,2)

endInd = 0.2

@time (grstar6, hrstar6) = henseltruncate(4,fr,gr,hr,sr,tr)
BivPolMod!(fr-grstar6*hrstar6,4)
plot_error_along_R(fq,grstar6*hrstar6, "n = 4", fq, 0, endInd, R) 

@time (grstar, hrstar6) = henseltruncate(8, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar6*hrstar6,8)
plot_error_along_R(fq,grstar6*hrstar6, "n = 8", fq, 0, endInd, R)


@time (grstar6, hrstar6) = henseltruncate(16, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar6*hrstar6,16)
plot_error_along_R(fq,grstar6*hrstar6, "n = 16", fq,  0, endInd, R)

@time (grstar6, hrstar6) = henseltruncate(32, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar6*hrstar6,32)
plot_error_along_R(fq,grstar6*hrstar6, "n = 32", fq, 0, endInd, R)


@time (grstar6, hrstar6) = henseltruncate(64, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar6*hrstar6,64)
plot_error_along_R(fq,grstar6*hrstar6, "n = 64", fq, 0, endInd, R)

@time (grstar6, hrstar6) = henseltruncate(128, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar6*hrstar6,64)
plot_error_along_R(fq,grstar6*hrstar6, "n = 128", fq, 0, endInd, R)

hensel_test(fr, gr, hr, fq, gq, hq, 7, R)

#################################
N = 14
fq = (y_q- (x_q^24 + x_q^18 -x_q-1)^N)*(y_q- (x_q^2 + x_q + 7))
gq = y_q- 7 
hq = y_q - 1

BivPolMod!(gq,1)
BivPolMod!(hq,1)

gi = convert_polynomial(gq, [RBXY,RBX], RR)
gr = convert_polynomial(gq,[RXY,RX],RDF)
hi = convert_polynomial(hq, [RBXY,RBX], RR)
hr = convert_polynomial(hq,[RXY,RX],RDF)
fi = convert_polynomial(fq, [RBXY,RBX], RR)
fr = convert_polynomial(fq,[RXY,RX],RDF)

deg = 128#128
@time (gqstar, hqstar) = henseltruncate(deg,fq,gq,hq)
@time (gistar, histar) = henseltruncate(deg,fi,gi,hi)
@time (gvstar, hvstar) = henseltruncate(deg,fi,gi,hi,validation = true)
@time (grstar, hrstar) = henseltruncate(deg,fr,gr,hr)
@time (grv, hrv) = validate_Hensel_a_posteriori(deg,fi,gi,hi);

norm1error(grstar,gqstar)
norm1error(hrstar,hqstar)
norm1error(gistar)
norm1error(histar)
norm1error(gvstar)
norm1error(hvstar)
norm1error(grv)
norm1error(hrv)

range_of_coefficients(fq)
range_of_coefficients(gq)
range_of_coefficients(hq)

error_measure1(gqstar,grstar, R/2)
error_measure1(hqstar,hrstar, R/2)
error_measure1(gistar, R/2)
error_measure1(histar, R/2)
error_measure1(gvstar, R/2)
error_measure1(hvstar, R/2)
error_measure1(grv, R/2)
error_measure1(hrv, R/2)
