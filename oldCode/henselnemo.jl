using Nemo
using Plots

#code based on Léo's code Hensel.jl


precision(BigFloat)
#setprecision(BigFloat, n) 
eps(BigFloat)

QQBar

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")
precision(CC)

C = CalciumField()

c = CC(1.0)
c = CC(1.0, 2)
typeof(c)
#convert(ComplexFieldElem, complex(1.0))
pc = CX()
ac = complex(7.0,2)
acc = CC(7.0, 2.5)

typeof(CXY)
typeof(pc)
set_coefficient!(pc,2,acc)
c = CC(7/5)

"""
    convert_to_complex_xy(p::PolyElem)

Convert a bivariate real or rational polynomial into a complex polynomial
"""
function convert_to_complex_xy(p::PolyElem)
    q = CXY()
    dp = degree(p)
    for i ∈ 1:dp+1
        m = degree(p.coeffs[i])
        set_coefficient!(q,i-1,1)
        print("q: ", q, "\n")
        #qi = CX()
        for j ∈ 1:m+1
            print(q, "\n")
            print(p, "\n")
            print("coeff(p.coeffs[i],j) ", coeff(p.coeffs[i],j-1), "\n")
            print(typeof(p.coeffs), "\t", typeof(q.coeffs), "\n")
            print("p.coeffs[i]", p.coeffs[i], "\n")
            print("q.coeffs[i]", q.coeffs[i], "\n")
            set_coefficient!(q.coeffs[i],j-1,CC(coeff(p.coeffs[i],j-1)))
            #set_coefficient!(qi,j-1,CC(coeff(p.coeffs[i],j-1)))
        end
        #set_coefficient!(q, i-1, qi)
    end
    q
end

"""
    convert_to_complex_x(p::PolyElem)

Convert a rational or real univariate polynomial to a complex polynomial.
"""
function convert_to_complex_x(p::PolyElem)
    q = CX()
    dp = degree(p)
    for i ∈ 1:dp+1
        set_coefficient!(q,i-1,CC(coeff(p,i-1)))
    end
    q
end

f = 3.0*y^2 + 10*x*y + 7.0
f.coeffs[1]
convert_to_complex_xy(f)

gc = 2.0*y_c^2 + 10*x_c*y_c + 7.0
gc.coeffs

a = 2.0*x^2 -5*x + 2
convert_to_complex_x(a)

"""
    BivPolMod(q::PolyElem, n::Int)

q mod x^n for bivariate polynomials
"""
function BivPolMod!(q::PolyElem, n::Int)
    dq = degree(q)
    for i ∈ 1:dq+1
        q.coeffs[i] = truncate(q.coeffs[i],n)
    end
    q    
end

"""
    newtoninversion(f::PolyElem, l::Int)

Invert the polynomial f through a Newton iteration such that fg=1 mod x^l
only works for unitary functions
"""
function newtoninversion(f::PolyElem, l::Int)
    #only works for unitary functions
    g = one(f)
    r = ceil(Int, log2(l))
    pow = 1
    print("\n", l, " ", r)
    for i ∈ 1:r
        pow *= 2
        g = 2g-mullow(f,g*g,pow)
        g = truncate(g,pow)
        print("-")
    end
    print("\"\n")
    g
end

"""
    fastdivrem(a::PolyElem, b::PolyElem)

Divide polynomial a by polynomial b with remainder and return q and r such that a = q*b + r.
"""
function fastdivrem(a::PolyElem, b::PolyElem)
    #print("a: ", a, "\n")
    da = degree(a)
    db = degree(b)
    da<db && return (zero(a),a)
    m = da-db
    invrevb = newtoninversion(reverse(b), m+1)
    reva = deepcopy(reverse(a)) #need to make a copy otherwise the following
                                #PolynomialModulo! changes the a as well
                                #reverse seems to work directly on the coefficients
    #print("a after reva: ", a, "\n")
    #PolynomialModulo!(reva, m+1)
    reva = truncate(reva, m+1)
    #print("a after reva mod: ", a, "\n")
    #print("reva: ", reva, "\n")
    #print("invrevb: ", invrevb, "\n")
    qstar = reva*invrevb
    #print("a after qstar: ", a, "\n")
    #PolynomialModulo!(qstar, m+1)
    qstar = truncate(qstar, m+1)
    #print("a after qstar mod: ", a, "\n")  
    q = reverse(qstar)
    #print("a after qstar reverse: ", a, "\n")
    r = a -b*q
    #print("a after r: ", a, "\n")
    #PolynomialModulo!(r, db)
    r = truncate(r, db)
    #print("a after r mod: ", a, "\n")
    q, r  
end

"""
    hensel_step_truncate(deg::Int, f::PolyElem, g::PolyElem, h::PolyElem, s::PolyElem, t::PolyElem)

A single Hensel lifting step.
"""
function hensel_step_truncate(deg::Int, f::PolyElem, g::PolyElem, h::PolyElem, s::PolyElem, t::PolyElem)
    print(deg, "-----------------\n")
    #print("f-gh = (", f, ") - (", g, ") * (", h, ") = ", f, " - ", g*h, "\n")
    #print("e: ", f-g*h, " (without mod)\n")
    e = f-g*h
    BivPolMod!(e, 2deg)
    #print("e: ", e, "\n")
    (q,r) = fastdivrem(s*e,h)
    #print("q: ", q, "\t r: ", r, "\n")
    n = degree(g)
    #print(g, " + ", t, "*", e, " + ", q, " * ", g, "\n")
    #print(" = ", g+t*e +q*g, "\n")
    gstar = g+t*e+q*g
    BivPolMod!(gstar, 2deg)
    #PolynomialModulo!(gstar, n+1)
    gstar = truncate(gstar, n+1)
    #print("g*: ", gstar, "\n")
    hstar = h+r
    BivPolMod!(hstar, 2deg)
    #print("h*: ", hstar, "\n")
    
    b = s*gstar + t*hstar-1
    BivPolMod!(b, 2deg)
    #print("b: ", b, "\n")
    sb = s*b
    BivPolMod!(sb, 2deg)
    #print("sb: ", sb, "\n")
    (c,d) = fastdivrem(sb, hstar)
    #print("c: ", c, "\n")
    #print("d: ", d, "\n")
    sstar = s-d
    BivPolMod!(sstar, 2deg)
    #print("sstar: ", sstar, "\n")
    tstar = t-t*b-c*gstar
    BivPolMod!(tstar, 2deg)
    #print("tstar: ", tstar, "\n")

    (2deg, gstar, hstar, sstar, tstar)
end

"""
    henseltruncate(l::Int, f::PolyElem, g::PolyElem, h::PolyElem, s::PolyElem, t::PolyElem)

Do a Hensel lifting on the polynomials g and h  such that f ≡ g* h* mod x^l

# Arguments
- `g, h` need to be given such that f≡gh mod x
- `s, t` need to be given such that sg + th ≡ 1 mod x
- `h` is monic
- `l` power of two
"""
function henseltruncate(l::Int, f::PolyElem, g::PolyElem, h::PolyElem, s::PolyElem, t::PolyElem)
    deg = 1
    while deg < l
        (deg, g, h, s, t) = hensel_step_truncate(deg, f, g, h, s, t)
    end
    BivPolMod!(g,l)
    BivPolMod!(h,l)
    (g,h)
end

"""
    hensel_step_full_term(deg::Int, f::PolyElem, g::PolyElem, h::PolyElem, s::PolyElem, t::PolyElem)

hensel_step() without truncating in y.
"""
function hensel_step_full_term(deg::Int, f::PolyElem, g::PolyElem, h::PolyElem, s::PolyElem, t::PolyElem)
    e = f-g*h
    BivPolMod!(e, 2deg)
    (q,r) = fastdivrem(s*e,h)
    n = degree(g)
    gstar = g+t*e+q*g
    BivPolMod!(gstar, 2deg)
    #gstar = truncate(gstar, n+1)
    hstar = h+r
    BivPolMod!(hstar, 2deg)
    
    b = s*gstar + t*hstar-1
    BivPolMod!(b, 2deg)
    sb = s*b
    BivPolMod!(sb, 2deg)
    (c,d) = fastdivrem(sb, hstar)
    sstar = s-d
    BivPolMod!(sstar, 2deg)
    tstar = t-t*b-c*gstar
    BivPolMod!(tstar, 2deg)

    (2deg, gstar, hstar, sstar, tstar)
end



"""
    hensel_full_term(l::Int, f::PolyElem, g::PolyElem, h::PolyElem, s::PolyElem, t::PolyElem)

henseltruncate() without truncating in y
"""
function hensel_full_term(l::Int, f::PolyElem, g::PolyElem, h::PolyElem, s::PolyElem, t::PolyElem)
    deg = 1
    while deg < l
        (deg, g, h, s, t) = hensel_step_full_term(deg, f, g, h, s, t)
    end
    BivPolMod!(g,l)
    BivPolMod!(h,l)
    (g,h)
end

"""
    subtract(p::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, q::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}})

Subtract a real polynomial from a rational one.
Result is in BigFloat (type: Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}) to accomodate for QQPolyRingElem allowing
bigger integers than Int64 and Float64 know how to handle
@todo: currently does not handle well if the first index of any of the coefficient polynomials is 0
"""
function subtract(p::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, q::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}})
    #@todo: see if I can do something with types T here for the floats
    dp = degree(p)
    dq = degree(q)
    dr = max(dp,dq)
    r = BFXY()
    v = collect(coefficients(p))
    w = collect(coefficients(q))
    for i ∈ 1:dr+1
        n = max(degree(v[i]), degree(w[i]))
        set_coefficient!(r,i-1,1)
        for j ∈ n+1:-1:1
            co = convert(Rational{BigInt}, coeff(p.coeffs[i],j))-coeff(q.coeffs[i],j) #this gives a conversion error if the 
            print(i-1, "\t", j-1, "\t", convert(Rational{BigInt}, coeff(p.coeffs[i],j-1)), "\t", coeff(q.coeffs[i],j-1), "\n")
            #print("co: ", co, "\n")
            #print(r.coeffs[i], "\n")
            set_coefficient!(r.coeffs[i], j, co) #doesn't work to set j=1 to be 0
            #print(i, "\t", j, "\t", coeff(r.coeffs[i],j-1), "\n")
        end
    end
    r
end

"""
    error_with_R(p_exact::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, p_approx::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, f::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, k=1//2)

Compute the error measure e = ∑|̄p_n-p_n|*(k*R)^n/∑|p_n|*(k*R)^n and R, with R the distance to the closest singularity in f 
"""
function error_with_R(p_exact::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, p_approx::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, f::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, k=1//2)
    dis = discriminant(f)
    #@show dis
    #print("discriminant: ", dis, "\n")
    if degree(dis) == 0
        #print("degree of the discriminant is 0")
    elseif degree(dis) == 1
        r = roots(dis, QQ)
        #R = convert(Rational{BigInt},R)
    else
        #print("deriv: ", derivative(dis), "\n")
        d = gcd(dis, derivative(dis))
        #print("d: ", d, "\n")
        if d == 1
            @show d
            #change_base_ring(RX, dis)
            r = roots(dis,RDF)#, QQ) 
        else
            #@show dis
            #@show d
            d = divexact(dis,d)
            #@show dis
            @show d
            r = roots(d)#,QQ) 
            #print(r,"\n")
        end        
    end
    @show r
    R = minimum(abs.(r))
    #print("R: ", R, "\n")
    R = convert(Rational{BigInt},R)
    e = error_measure1(p_exact,p_approx, k*R)
    (e,R)  
end

"""
    error_measure1(p_exact::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, p_approx::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, R=Rational{BigInt}(1))

∑|̄p_n-p_n|*R^n/∑|p_n|*R^n 
"""
function error_measure1(p_exact::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, p_approx::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, R=Rational{BigInt}(1))
    #print("\n \n")
    num = 0
    den = 0
    dp = degree(p_approx)
    for i ∈ 1:dp+1
        dpi = degree(p_approx.coeffs[i])
        for j ∈ 1:dpi+1
            #print("num += ", coeff(p_approx.coeffs[i],j-1), "\t - \t", convert(Rational{BigInt}, coeff(p_exact.coeffs[i],j-1)), "\t = \t", coeff(p_approx.coeffs[i],j)-convert(BigFloat, convert(Rational{BigInt}, coeff(p_exact.coeffs[i],j-1))), "\n abs =", abs(coeff(p_approx.coeffs[i],j)-convert(Rational{BigInt}, coeff(p_exact.coeffs[i],j-1)))*R^(j-1), "\n")
            numtemp = coeff(p_approx.coeffs[i],j-1)-convert(Rational{BigInt}, coeff(p_exact.coeffs[i],j-1))
            #@show numtemp
            num += (abs(numtemp))*R^(j-1)
            #print( abs(numtemp)*R^(j-1), "\n")
            #print("den += ", abs(convert(Rational{BigInt}, coeff(p_exact.coeffs[i],j-1)))*R^(j-1), "\n")
            #print(coeff(p_exact.coeffs[i],j-1), "\n")
            dentemp = convert(Rational{BigInt}, coeff(p_exact.coeffs[i],j-1))
            den += abs(dentemp)*R^(j-1)
            #print("num = ", num, "\n")
        end
    end
    #print("num = ", num, ", den = ", den, "\n")
    #@show num
    num/den
end

N=14
fr =  y^2 - (5^N+1)*y + (x^34 -x + 1)^N*(x^13 -27*x+5//1)^N
fq =  y_q^2 - (5^N+1)*y_q + (x_q^34 -x_q + 1)^N*(x_q^13 -27*x_q+5//1)^N
#plot()
#plot_error_along_R!(fq,fr, "f to f")
error_measure1(fq,fr)


"""
    plot_error_along_R(pq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, pr::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, l =  "error along R", fq=pq, startIndex = 0, endIndex = 3)

Plot ∑|pr_n-pq_n|*r^n/∑|pq_n|*r^n for r ∈ [startIndex, endIndex] and show the convergence radius R.
"""
function plot_error_along_R(pq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, pr::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, l =  "error along R", fq=pq, startIndex = 0, endIndex = 3)
    (_,R) = error_with_R(pq,pr,fq)
    @show R
    rrange = range(startIndex,endIndex,200) #@todo: add these bounds as arguments?
    #print("rrange= ", rrange, "\n")
    #map(u->print(u, "\n"), rrange)
    plot(rrange, map(u->(error_measure1(pq,pr,u)), rrange), label = l)
    
    scatter!([R],[0], label = "R")
end

"""
    plot_error_along_R!(pq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, pr::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, l = "error along R", startIndex=0, endIndex = 3)

Add ∑|pr_n-pq_n|*r^n/∑|pq_n|*r^n for r ∈ [startIndex, endIndex] to the current plot.
"""
function plot_error_along_R!(pq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, pr::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, l = "error along R", startIndex=0, endIndex = 3)
    rrange = range(startIndex,endIndex, 200)
    plot!(rrange, map(u->(error_measure1(pq,pr,u)), rrange), label = l)
end

d = 7*x_q^3 +6*x_q^2 +5*x_q + 3
roots(d)
roots(d,QQ)

d = x_q^3 +6*x_q^2 +5*x_q + 3
roots(d)
roots(d,QQBar)
#roots(d, C)

"""
    hensel_test(f::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, g::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, h::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, fq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, gq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, hq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, l::Int)

Compute the relative error of the Hensel lifting steps in Float64 for l steps, draw a plot of the errors of g*h, g, and h, after each step and return the error values at R/2 for each of them.
    # Arguments
- `f, g, h` polynomials in Float64 on which the Hensel lifting is to be done 
- `fq, gq, hq` the same polynomials in rational for comparison
- `l` number of lifting steps
"""
function hensel_test(f::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, g::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, h::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}, fq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, gq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, hq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, l::Int)
    BivPolMod!(g,1)
    BivPolMod!(h,1)
    (_,s,t) = gcdx(g,h)
    (_, sq, tq) = gcdx(gq,hq)
    deg = 1
    plot(label = "hensel_test")
    i = 1
    egh = zeros(BigFloat,l)
    eg = zeros(BigFloat, l)
    eh = zeros(BigFloat, l)
    d = zeros(Int, l)
    while deg < 2^l
        de = deg
        print(deg, "\n")
        (deg, g, h, s, t) = hensel_step_truncate(deg, f, g, h, s, t)
        (_, gq, hq, sq, tq) = hensel_step_truncate(de, fq, gq, hq, sq, tq) 
        #print(BivPolMod!(f-g*h,deg), "\n")
        #display(plot_error_along_R!(fq, g*h, string("n = ", deg),0,1.3))
        (egh[i],R) = error_with_R(fq,g*h,fq)
        #(egh[i]) = error_measure1(fq, g*h, 1.1)
        (eg[i]) = error_measure1(gq,g, R/2) #R/2
        (eh[i]) = error_measure1(hq, h, R/2) #R/2
        #display(plot_error_along_R(fq, g*h, string("g*h","n = ", deg),fq))
        #display(plot_error_along_R!(gq*hq, g*h, string("g*h, gq*hq", "n = ", deg)))
        display(plot_error_along_R(gq, g, "g",fq))
        display(plot_error_along_R!(hq, h, "h"))
        d[i] = deg
        i += 1
    end
    #@show egh
    @show eg
    @show eh
    #display(plot(d,egh, label = "error of g*h at R/2"))
    display(plot(d,eg, label = "error of g at R/2"))
    display(plot!(d, eh, label = "error of h at R/2"))
    #display(plot!(xlims =[16,256] ))
    #display(plot!(ylims = [0, 0.02]))
    (eg, eh)
end


# example functions
g = y -(2*x^3 -5*x^2 + 8*x -5)
h= y - (x^3 -3* x^2 + 3*x -1)
f1 = y^2 + 10*x +5
f2 = y^2 + 10*y +5
k = 7*x^2 +10//1*x +5//3
#polynomial types
typeof(f1)
typeof(k)

#discriminant
discriminant(g)
discriminant(h)
discriminant(f1)
discriminant(f2)
discriminant(k)

#############################
#trials for type conversions#
#############################
N = 14
k = QX([5,10,7])
g0 = QX([5,-8,5,-2])
typeof(g0)
g1 = QX([1])
g = QXY([g0,g1])
typeof(g)

g = (y_q - (x_q^2+2*x_q +5//2)^N)
h = (y - (x^2+2*x +5//2)^N)
typeof(g)
typeof(h)

k = QXY()
k = (y - (x^2+2*x +5//2)^N)
typeof(k)

#g = QXY()
#h = RXY() #only needed if we change the type from a previous definition
typeof(g)
g = (y - (x^2+2*x +5//2)^N)
h = (y_q - (x_q^2+2*x_q +5//2)^N)
g == h
typeof(g)
typeof(h)
#g-h

typeof(5//2)
typeof(5/2)
typeof(5//2) == typeof(5/2)
5//2 == 5/2
5//2 - 5/2
typeof(5//2 - 5/2)
#coeff(g.coeffs[1],1) == coeff(h.coeffs[1],1) #ERROR: InexactError: convert(ZZRingElem, -4.1723251342773438e6)
#coeff(h.coeffs[1],1) == coeff(g.coeffs[1],1) #same error
#coeff(g.coeffs[1],1) - coeff(h.coeffs[1],1) #saaame

k = RXY()
k = g
typeof(k)
typeof(coeff(h.coeffs[1],1))
convert(Rational{Int64}, coeff(h.coeffs[1],1))
typeof(coeff(g.coeffs[1],1))

convert(Rational{Int64}, coeff(h.coeffs[1],1)) == coeff(g.coeffs[1],1) #ERROR: InexactError: convert(ZZRingElem, -4.1723251342773438e6)
coeff(g.coeffs[1],1) == convert(Rational{Int64}, coeff(h.coeffs[1],1)) #same error
convert(Rational{Int64}, coeff(h.coeffs[1],1)) - coeff(g.coeffs[1],1)

T = promote_type(typeof(h),typeof(g))
gprime = convert(T,g)
hprime = convert(T,h)
typeof(gprime) == typeof(hprime)

g
h
l = subtract(h,g)
g
h


u = 5//2
typeof(u)
convert(Float64,u)
u
j = RXY()
j
set_coefficient!(j,2, 1)
j
set_coefficient!(j, 4, 1)
set_coefficient!(j.coeffs[4], 2, 7)
j

m = RXY()
set_coefficient!(m, 2, 0)

#coefficients
m = coeff(coeff(f1,0),1)
f1.coeffs

#Tests for PolynomialModulo and BivPolMod

var(parent(k))

km = deepcopy(k)
#PolynomialModulo!(km,1)
truncate(km,1)
km2 = deepcopy(k)
#PolynomialModulo!(km2,2)
truncate(km2, 2)
km3 = deepcopy(k)
#PolynomialModulo!(km3,3)
truncate(km3,3)
#km = PolynomialModulo(k,1)
#km = PolynomialModulo(k, 2)
#km = PolynomialModulo(k,3)
km2

f1 = y^2 + 10*x +5
f2 = y^2 + 10*y +5

f2
fm2 = deepcopy(f2)
BivPolMod!(fm2,2)
f2

f1
fm1 = deepcopy(f1)
BivPolMod!(fm1,1)
f1

f3 = (5*x^2 + 7*x)*y^2 + 2*x^2 + 10*x +5
fm3 = deepcopy(f3)
BivPolMod!(fm3,2)

fm4 = deepcopy(f3)
#PolynomialModulo!(fm4,1)
truncate(fm4,1)

#test gcdx
g = y -(2*x^3 -5*x^2 + 8*x -5)
h= y - (x^3 -3* x^2 + 3*x -1)
gprime = deepcopy(g)
BivPolMod!(gprime,1)
hprime = deepcopy(h)
BivPolMod!(hprime,1)
(d,s,t) = gcdx(gprime,hprime)
g
h

#test newtoninversion
g = y -(2*x^3 -5*x^2 + 8*x -5)
ginv = newtoninversion(g, degree(g))
g
ginv == 1
finv = newtoninversion(f2, degree(f2))
finv == -10*y -3

#test rev
g
reverse(g,5)
f1

reverse(f1,1)
reverse(g)
g
reverse(f1)

#divrem test
g = y -(2*x^3 -5*x^2 + 8*x -5)
h= y - (x^3 -3* x^2 + 3*x -1)
divrem(g,h)
g
h
g = y -(2*x^3 -5*x^2 + 8*x -5)
h= y - (x^3 -3* x^2 + 3*x -1)
fastdivrem(g,h)
g
h

#subtract tries
b = 7*x + 4*x^2
set_coefficient!(b, 1, 0)
set_coefficient!(b, 7, 2)

w = b*y + (6*x +2)*y^2 + 2
w.coeffs
set_coefficient!(w.coeffs[1], 3, 2)
w
set_coefficient!(w.coeffs[2], 1, 0)
set_coefficient!(w.coeffs[2], 2, 0)
w
convert_to_complex_xy(w)

w =RXY()
set_coefficient!(w, 1, 1)
set_coefficient!(w.coeffs[1], 1, 0)
w
set_coefficient!(w.coeffs[1], 2, 7.0)
w
set_coefficient!(w.coeffs[2], 1, 2)
w
set_coefficient!(w.coeffs[2], 0, 0.0)
#set_coefficient!(w,1, 0)
w



#####################################
#hensel trials
###########
#example 1#
###########
gr = y -(2*x^3 -5*x^2 + 8*x -5)
hr = y - (x^3 -3* x^2 + 3*x -1)
gr*hr
fr = y^2 + 6*y +5 -10*x

#rational functions for error comparison
gq = y_q -(2*x_q^3 -5*x_q^2 + 8*x_q -5)
hq = y_q - (x_q^3 -3* x_q^2 + 3*x_q -1)
fq = y_q^2 + 6*y_q +5 -10*x_q

grprime = deepcopy(gr)
BivPolMod!(grprime,1)
hrprime = deepcopy(hr)
BivPolMod!(hrprime,1)
(d, sr, tr) = gcdx(grprime, hrprime)

BivPolMod!(gq,1)
BivPolMod!(hq,1)
(d, sq, tq) = gcdx(gq,hq)

gh = g*h
BivPolMod!(gh,1)
gprime*hprime
g

@time (grstar, hrstar) = henseltruncate(1, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar*hrstar,1)
#@time (gqstar, hqstar) = henseltruncate(1, fq, gq, hq, sq, tq)
#error_with_R(fq, grstar*hrstar, fq)
#fq
#grstar*hrstar
#subtract(fq, grstar*hrstar) #gives me -10 when it should be -10x
#plot_error_along_R(fq,grstar*hrstar)

@time (grstar, hrstar) = henseltruncate(2, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar*hrstar,2)

@time (grstar, hrstar) = henseltruncate(4, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar*hrstar,4)

@time (grstar, hrstar) = henseltruncate(8, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar*hrstar,8)

@time (grstar, hrstar) = henseltruncate(16, fr, gr, hr, sr, tr)
fr - grstar*hrstar
BivPolMod!(fr-grstar*hrstar,16)

@time (grstar_full, hrstar_full) = hensel_full_term(16, fr, gr, hr, sr, tr)
fr - grstar_full*hrstar_full
BivPolMod!(fr-grstar_full*hrstar_full,16)

@time (gqstar, hqstar) = henseltruncate(16, fq, gq, hq, sq, tq)
error_with_R(fq, grstar*hrstar, fq)
plot_error_along_R(fq,grstar*hrstar, "grstar*hrstar", fq, 0,0.5)

@time (grstar, hrstar) = henseltruncate(128, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar*hrstar,128)
error_with_R(fq, grstar*hrstar, fq)
plot_error_along_R(fq,grstar*hrstar, "grstar*hrstar", fq, 0, 0.42)

hensel_test(fr, gr, hr, fq, gq, hq, 7)

#example 1 slightly to the left (example 2)
gr2 = y -(2*x^3 -5*x^2 + (8-1e-6)*x -5)
hr2= y - (x^3 -3.1* x^2 + 3*x -1.1) #y - (x^3 -(3+1e-9)* x^2 + 3*x -1)
BivPolMod!(gr2,1)
BivPolMod!(hr2,1)
(d,sr2,tr2) = gcdx(gr2,hr2)

@time (grstar2, hrstar2) = henseltruncate(1, fr, gr2, hr2, sr2, tr2)
BivPolMod!(fr-grstar2*hrstar2,1)

@time (grstar2, hrstar2) = henseltruncate(2, fr, gr2, hr2, sr2, tr2)
BivPolMod!(fr-grstar2*hrstar2,2)

@time (grstar2, hrstar2) = henseltruncate(4, fr, gr2, hr2, sr2, tr2)
BivPolMod!(fr-grstar2*hrstar2,4)

@time (grstar2, hrstar2) = henseltruncate(8, fr, gr2, hr2, sr2, tr2)
BivPolMod!(fr-grstar2*hrstar2,8)

@time (grstar2, hrstar2) = henseltruncate(16, fr, gr2, hr2, sr2, tr2)
BivPolMod!(fr-grstar2*hrstar2,16)

@time (grstar2, hrstar2) = henseltruncate(128, fr, gr2, hr2, sr2, tr2)
BivPolMod!(fr-grstar2*hrstar2,128)

grstar-grstar2
hrstar-hrstar2

h22 = (1-1e-4)*y - (x^3 -3* x^2 + 3*x -1)
h22prime = deepcopy(h22)
BivPolMod!(h22prime,1)
(d,s22, t22) = gcdx(gr2, h22prime)
@time (gstar22, hstar22) = henseltruncate(8,fr, gr2, h22, s22, t22)
BivPolMod!(f-gstar22*hstar22,8)
grstar-gstar22
hrstar-hstar22


#################################################
#example 3
N = 14
Fr = (y^2 -1 - x^2 + 3*x^8 - 5*x^24)^N + x^N*y
#F = (y^2-1)^N
gr3 = (y-1)^N
hr3 = (y+1)^N

Fq = (y_q^2 -1 - x_q^2 + 3*x_q^8 - 5*x_q^24)^N + x_q^N*y_q
gq3 = (y_q-1)^N
hq3 = (y_q+1)^N

Frprime = deepcopy(Fr)
BivPolMod!(Frprime, 1)
gh3 = gr3*hr3
BivPolMod!(gh3,1)
Frprime == gh3
Frprime - gh3
gr3prime = deepcopy(gr3)
hr3prime = deepcopy(hr3)
BivPolMod!(gr3prime,1)
BivPolMod!(hr3prime,1)
#(dr3, sr3, tr3) = gcdx(gr3prime,hr3prime)
(dr3, sr3, tr3) = gcdx(gr3,hr3)

@time (grstar3, hrstar3) = henseltruncate(2, Fr, gr3, hr3, sr3, tr3) 
BivPolMod!(Fr-grstar3*hrstar3,2)

@time (grstar3, hrstar3) = henseltruncate(4, Fr, gr3, hr3, sr3, tr3) 
BivPolMod!(Fr-grstar3*hrstar3,4)

#plot()
#plot_error_along_R(Fq, grstar3*hrstar3, "gstar*hstar, n = 4")
plot_error_along_R(gq3, grstar3, "gstar n = 4", Fq, 0, 0.01)
plot_error_along_R!(hq3, hrstar3, "hstar n = 4", 0, 0.01)

@time (grstar3, hrstar3) = henseltruncate(8, Fr, gr3, hr3, sr3, tr3)
BivPolMod!(Fr-grstar3*hrstar3,8)
plot_error_along_R(gq3, grstar3, "gstar n = 8", Fq, 0, 0.01)
plot_error_along_R!(hq3, hrstar3, "hstar n = 8", 0, 0.01)


@time (grstar3, hrstar3) = henseltruncate(16, Fr, gr3, hr3, sr3, tr3) 
BivPolMod!(Fr-grstar3*hrstar3,16)
#plot()
#plot_error_along_R!(Fq, grstar3*hrstar3, "grstar*hrstar, n = 16", 0, 0.0001)
plot_error_along_R(gq3, grstar3, "gstar n = 16", Fq, 0, 0.0001)
plot_error_along_R!(hq3, hrstar3, "hstar n = 16", 0, 0.0001)

#@time (gstar3, hstar3) = henseltruncate(32, F, g3, h3, s3, t3) 
#BivPolMod!(F-gstar3*hstar3,32)

#@time (gstar3, hstar3) = henseltruncate(128, F, g3, h3, s3, t3)
#BivPolMod!(F-gstar3*hstar3,128)
@time 1+1

hensel_test(Fr, gr3, hr3, Fq, gq3, hq3, 4)

###################################
#example 4
N = 14
gr = (y - (x^2+2*x +5//1)^N)
hr = (y - (2*x^2 -7*x +1//1)^N)
fr =  y^2 - (5^N+1)*y + (x^34 -x + 1)^N*(x^13 -27*x+5//1)^N
BivPolMod!(fr-gr*hr,1)


gq = (y_q - (x_q^2+2*x_q +5//1)^N)
hq = (y_q - (2*x_q^2 -7*x_q +1//1)^N)
fq =  y_q^2 - (5^N+1)*y_q + (x_q^34 -x_q + 1)^N*(x_q^13 -27*x_q+5//1)^N

#gprime = deepcopy(g)
#hprime = deepcopy(h)
BivPolMod!(gr,1)
BivPolMod!(hr,1)
(dr,sr,tr) = gcdx(gr,hr)
(dq, sq, tq) = gcdx(gq, hq)

hr
gr

@time (grstar4, hrstar4) = henseltruncate(2, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar4*hrstar4,2)

@time (grstar4, hrstar4) = henseltruncate(4,fr,gr,hr,sr,tr)
BivPolMod!(fr-grstar4*hrstar4,4)

@time (grstar4, hrstar4) = henseltruncate(8, fr, gr, hr, sr, tr)
(gqstar4, hqstar4) = henseltruncate(8, fq, gq, hq, sq, tq)
BivPolMod!(fr-grstar4*hrstar4,8)
fq
plot()
#plot_error_along_R!(fq, grstar4*hrstar4, "gstar*hstar to f, n = 8", 0, 1) #line 744 unable to isolate all roots gcd(p, p')=1, so
grstar4*hrstar4
gqstar4*hqstar4
plot_error_along_R!(gqstar4*hqstar4, grstar4*hrstar4, "gstar*hstar, n = 8", 0,1)
#error_with_R(fq,grstar4*hrstar4,fq)
-6.103515626e9 - (-6103515626//1)

@time (grstar4, hrstar4) = henseltruncate(16, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar4*hrstar4,16)

@time (grstar4, hrstar4) = henseltruncate(32, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar4*hrstar4,32)

@time (grstar4, hrstar4) = henseltruncate(64, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar4*hrstar4,64)

@time (grstar4, hrstar4) = henseltruncate(128, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar4*hrstar4,128)
#@todo: add more plot_error_along_R here once I fixed the problem with the roots, also for g* and h*

#it works with QQBar, but that crashes Visual Studio for big f

#error_with_R(gq,grstar4,fq)
change_base_ring(RDF, gq)
change_base_ring(QQ, gr)
hensel_test(fr, gr, hr, fq, gq, hq, 7) #discriminant has no real roots (or, well, at least no rational ones)

###############
#Léo's example#
###############
#example 5
fr0 = 1 +2*x^2 +x^3
fr1 = x + 3* x^2
fr2 = -1 +2*x^2 + 2*x^3
fr = (3*fr0) + fr1*y + 3*fr2*y^2

gr = 3*(1//1 -y)
g2r = 3*(1.0 -(1.0-3e-5)*y)
g2q = 3*(1 -(1 -1//(10^5))*y_q)

hr = y+1

fq0 = 1 +2*x_q^2 +x_q^3
fq1 = x_q + 3* x_q^2
fq2 = -1 +2*x_q^2 + 2*x_q^3
fq = (3*fq0) + fq1*y_q + 3*fq2*y_q^2

gq = 3*(1-y_q)
hq = y_q +1


BivPolMod!(fr-gr*hr,1)
BivPolMod!(fr- g2r*hr,1)

(d, sr, tr) = gcdx(gr,hr)
(d, sr2, tr2) = gcdx(gr2, hr)
(d, sq, tq) = gcdx(gq,hq)
(d, s2q, t2q) = gcdx(g2q, hq)

@time (grstar, hrstar) = henseltruncate(128, fr, gr, hr, sr, tr)
#@time (grstar2, hrstar2) = henseltruncate(128, fr, gr2, hr, sr2, tr2)
#####@time (gqstar2, hqstar2) = henseltruncate(128, fq, g2q, hq, s2q, t2q)
#@time (grstar_full, hrstar_full) = hensel_full_term(128,fr,gr,hr,sr,tr)

BivPolMod!(fr-grstar*hrstar,128)
BivPolMod!(fr-grstar2*hrstar2, 128)

@time (gqstar, hqstar) = henseltruncate(128, fq, gq, hq, sq, tq)
@time (gqstar_full, hqstar_full) = hensel_full_term( 128, fq, gq, hq, sq, tq)

(e,R) = error_with_R(fq, grstar*hrstar, fq) #no rational roots
plot_error_along_R(fq, grstar*hrstar, "gstar * hstar to f", fq, 0, 0.575)
#plot!(xlims = [0,0.5])
#plot()
gq
grstar
(e,R) = error_with_R(gq, grstar, fq)
plot_error_along_R!(gq, grstar, "gstar",0,0.575)
(e,R) = error_with_R(hq, hrstar, fq)
plot_error_along_R!(hq, hrstar, "hstar", 0, 0.575)
#plot_error_along_R!(fq, grstar2*hrstar2)

hensel_test(fr, gr, hr, fq, gq, hq, 7)

#############
##Example 6##
#############
fr = (y- (x^24 + x^18 -x-1)^N)*(y- (x^2 + x + 7))
gr = y- 7 
hr = y - 1

fq = (y_q- (x_q^24 + x_q^18 -x_q-1)^N)*(y_q- (x_q^2 + x_q + 7))
gq = y_q- 7 
hq = y_q - 1

BivPolMod!(fr-gr*hr,1)
(d, sr, tr) = gcdx(gr,hr)

@time (grstar6, hrstar6) = henseltruncate(2, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar6*hrstar6,2)

endInd = 0.2
plot()
@time (grstar6, hrstar6) = henseltruncate(4,fr,gr,hr,sr,tr)
BivPolMod!(fr-grstar6*hrstar6,4)
plot_error_along_R(fq,grstar6*hrstar6, "n = 4", fq, 0, endInd) #unable to isolate all roots

plot()
@time (grstar, hrstar6) = henseltruncate(8, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar6*hrstar6,8)
plot_error_along_R!(fq,grstar6*hrstar6, "n = 8", 0, endInd)

plot()
@time (grstar6, hrstar6) = henseltruncate(16, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar6*hrstar6,16)
plot_error_along_R!(fq,grstar6*hrstar6, "n = 16", 0, endInd)

#plot()
@time (grstar6, hrstar6) = henseltruncate(32, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar6*hrstar6,32)
plot_error_along_R!(fq,grstar6*hrstar6, "n = 32", 0, endInd)

#plot()
@time (grstar6, hrstar6) = henseltruncate(64, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar6*hrstar6,64)
plot_error_along_R!(fq,grstar6*hrstar6, "n = 64", 0, endInd)

#plot()
@time (grstar6, hrstar6) = henseltruncate(128, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar6*hrstar6,64)
plot_error_along_R!(fq,grstar6*hrstar6, "n = 128", 0, endInd)

hensel_test(fr, gr, hr, fq, gq, hq, 7)

#################
####Example 7####
#################
K = 20
fr = (3*y - (x-2)^K)*(y -(x^2+4)^K) 
gr = 3*y - 2^K 
hr = y - 4^K

fq = (3*y_q - (x_q-2)^K)*(y_q -(x_q^2+4)^K)
gq = 3*y_q - 2^K 
hq = y_q - 4^K

gr
BivPolMod!(fr-gr*hr,1)
gr

(d,sr,tr) = gcdx(gr,hr)

 
@time hensel_test(fr,gr,hr,fq,gq, hq, 8)
#plot!(ylims = [0, 0.0001])
(e, R) = error_with_R(fq,g*h,fq)
scatter!([R],[0], label = "R")
@time (grstar7,hrstar7) = henseltruncate(128, fr, gr, hr, sr, tr)
BivPolMod!(fr-grstar7*hrstar7, 128)

#example to see if the rational real conversion works before I rewrite the other examples:
#Example 8:
K = 20
#real
fr = (3*y - (x-2)^K)*(y -(x^2+4)^K) 
gr = 3*y - 2^K 
hr = y - 4^K

#rational
fq = (3*y_q - (x_q-2)^K)*(y_q -(x_q^2+4)^K) 
gq = 3*y_q - 2^K 
hq = y_q - 4^K

(d,sq,tq) = gcdx(gq,hq)
(d,sr,tr) = gcdx(gr,hr)

@time (gstarq,hstarq) = henseltruncate(128, fq, gq, hq, sq, tq)
@time (gstarr,hstarr) = henseltruncate(128, fr, gr, hr, sr, tr)

subtract(gstarq, gstarr)
subtract(hstarq, hstarr)

frprime = gstarr*hstarr
fqprime = gstarq*hstarq
subtract(fqprime, frprime)
abs(5//2)
error_measure1(gstarq,gstarr)
error_measure1(hstarq,hstarr)
error_measure1(fqprime,frprime)
error_measure1(fq, frprime)    

error_with_R(gstarq,gstarr,fq)
error_with_R(hstarq,hstarr,fq)
error_with_R(fq, gstarr*hstarr,fq)

range(0,2,200)
range(0,3,200)


(e,R) = error_with_R(fq,frprime,fq)
#e = convert(Float64, e)
#R = convert(Float64, R)
error_measure1(fq,frprime,R/2)

error_measure1(fq,frprime, 1/2)

rrange = range(0,1.34,200)
plot(rrange, map(u-> (error_measure1(gstarq,gstarr,u)), rrange), label = "gstar")
scatter!([R],[0], label = "R")

plot!(rrange, map(u-> (error_measure1(fq,frprime,u)), rrange), label = "gstar*hstar to f")

#scatter!([R],[0])
#scatter!([0.23647],[0])
#scatter!([1.79522655],[0])

plot!(rrange, map(u-> (error_measure1(hstarq,hstarr,u)), rrange), label = "hstar")
#scatter!([R],[0])


testr = 3*x*y + x +3
testq = 3*x_q*y_q +3
error_measure1(testq,testr)

hensel_test(fr, gr, hr, fq, gq, hq, 7)

#############
##Example 9##
#############
## (y^5-1)*(y^5+2)+x
fr = (y^5-1)*(y^5+2)+x
gr = y^5-1
hr = y^5+2

fq = (y_q^5-1)*(y_q^5+2)+x_q
gq = y_q^5 -1
hq = y_q^5 +2

hensel_test(fr,gr,hr,fq,gq,hq,4)

#a lot of the discriminants now have no rational roots, but when I try to change the field to Floats, it throws me an arrow that he can't convert an object of type QQFieldElem to a Float64 polynomial, despite the fact that there exists a constructor for Rationals (but not for QQFieldElem specifically)
