using Polynomials

#code based on Léo's code Hensel.jl

"""
    PolynomialModulo(p,n)
    BivPolMod(q,n)


p mod x^n for univariate and bivariate polynomials respectively
"""
PolynomialModulo(p,n)=Polynomial(p[0:n-1], Polynomials.indeterminate(p))
BivPolMod(q,n) = map(p->PolynomialModulo(p,n),q)

"""
    newtoninversion(f,l)

Invert the polynomial f through a Newton iteration such that fg=1 mod x^l
only works for unitary functions
"""
function newtoninversion(f,l)
    #only works for unitary functions
    g = one(f)
    r = ceil(Int, log2(l))
    pow = 1
    print("\n", l, " ", r)
    for i ∈ 1:r
        pow *= 2
        f2 = PolynomialModulo(f,pow)
        g = PolynomialModulo(2g-f2*g*g, pow) #here the polynomial multiplication is probably the thing that is taking so long
        print("-")
    end
    print("\"\n")
    g
end

"""
    rev(p,d)


Compute the reversal of polynomial p.
rev_k(p) = x^k p(1/x)
"""
function rev(p,d)
	t = zeros(eltype(p), d+1)
	pd = Polynomials.degree(p)
    pd == -1 && return Polynomial([0], Polynomials.indeterminate(p))
	t[d-pd+1:d+1] = reverse(p.coeffs)

	Polynomial(t,Polynomials.indeterminate(p))
end


"""
    fastdivrem(a,b)

Divide polynomial a by polynomial b with remainder and return q and r such that a = q*b + r.
"""
function fastdivrem(a,b)
    #only works for unitary functions
    da = Polynomials.degree(a)
    db = Polynomials.degree(b)
    da<db && return (zero(a),a)
    m = da-db
    invrevb = newtoninversion(rev(b,db), m+1)
    reva = PolynomialModulo(rev(a,da), m+1)
    #print("reva: ", reva, "\n")
    #print("invrevb: ", invrevb, "\n")
    #print("reva*invrevb: ", reva*invrevb, "\n")
    #print("m+1: ", m+1, "\n")
    #print("variable: ", Polynomials.indeterminate(reva*invrevb), "\n")
    qstar = reva*invrevb
    qstar = PolynomialModulo(qstar, m+1)
    #print("qstar: ", qstar, "\n")
    q = rev(qstar, m)
    q, PolynomialModulo(a-b*q, db)
end


"""
    divrem(a,b)

Divide polynomial a by polynomial b with remainder and return q and r such that a = q*b + r.
"""
function divrem(a,b)
	#type conversions to assert that a and b have the same type of polynomial
	T = promote_type(typeof(a), typeof(b))
	a = convert(T, a)
	b = convert(T, b)

	#set initial r
	r = a

	da = Polynomials.degree(a)
	db = Polynomials.degree(b)

	u = newtoninversion(b,db)
	if da >= db
		i = da-db
		q = Polynomial(zeros(i), Polynomials.indeterminate(a))
		q = convert(T, q)
		while i > 0
			dr = Polynomials.degree(r)
			if dr == db + i
				lcr = Polynomial(r.coeffs[dr+1], Polynomials.indeterminate(r.coeffs[dr+1])) #lc(r) is good
				lcrpoly = Polynomial([lcr,0], Polynomials.indeterminate(u))

				qi = u * lcrpoly
				q.coeffs[i] = qi.coeffs[1]

				#define y^i
				temp = zeros(i)
				temp[i] = 1.0
				ptemp = Polynomial(temp, Polynomials.indeterminate(a))
				ptemp = convert(T,ptemp)
                
				r = r - qi*ptemp*b
			else
				q.coeffs[i] = 0
			end		
			i = i-1
		end
	else
		print("error in division with remainder: polynomial 2 has bigger degree then polynomial 1\n")
		q = Polynomial([0],Polynomials.determinate(a))
	end 
	q, r
end

"""
    hensel_step_truncate(deg, f, g, h, s, t)

A single Hensel lifting step.
"""
function hensel_step_truncate(deg, f, g, h, s, t)
    #print(deg, "-----------------\n")
    #print("f-gh = (", f, ") - (", g, ") * (", h, ") = ", f, " - ", g*h, "\n")
    #print("e: ", f-g*h, " (without mod)\n")
    e = BivPolMod(f-g*h,2deg)
    #print("e: ", e, "\n")
    (q,r) = fastdivrem(s*e,h)
    #print("q: ", q, "\t r: ", r, "\n")
    n = Polynomials.degree(g)
    #print(g, " + ", t, "*", e, " + ", q, " * ", g, "\n")
    #print(" = ", g+t*e +q*g, "\n")
    gstar = BivPolMod(g+t*e + q*g, 2deg)
    gstar = PolynomialModulo(gstar, n+1) #on tronque les zeros parce que il y a des annulations dans t*e + q*g
    hstar = BivPolMod(h+r, 2deg)
    #print("g*: ", gstar, "\n")
    #print("h*: ", hstar, "\n")

    b = BivPolMod(s*gstar + t*hstar-1, 2deg)
    sb = BivPolMod(s*b, 2deg)
    (c,d) = fastdivrem(sb,hstar)
    sstar = BivPolMod(s-d, 2deg)
    tstar = BivPolMod(t-t*b-c*gstar, 2deg)
    #print("b: ", b, "\n")
    #print("sb: ", sb, "\n")
    #print("c: ", c, "\n")
    #print("d: ", d, "\n")
    #print("sstar: ", sstar, "\n")
    #print("tstar: ", tstar, "\n")

    (2deg, gstar, hstar, sstar, tstar)
end


"""
    henseltruncate(l, f, g, h, s, t)

Do a Hensel lifting on the polynomials g and h  such that f ≡ g* h* mod x^l

# Arguments
- `g, h` need to be given such that f≡gh mod x
- `s, t` need to be given such that sg + th ≡ 1 mod x
- `h` is monic
"""
function henseltruncate(l, f, g, h, s, t)
    deg = 1
    #n = Polynomials.degree(g) #why did he call on this n here?
    while deg < l
        (deg, g, h, s, t) = hensel_step_truncate(deg,f, g, h, s, t)
    end
    (BivPolMod(g,l),BivPolMod(h,l))
end


#tests
g = Polynomial([-Polynomial([-5,8,-5,2//1]),1], :y)
h = Polynomial([-Polynomial([-1,3,-3,1//1]),1],:y)
f = Polynomial([5,10,1//1],:y)
f = convert(typeof(g), f)
s = Polynomial(Polynomial(-1//4), :y)
t = Polynomial(Polynomial(1//4), :y)
(gstar, hstar) = henseltruncate(8,f,g,h,s,t) #overflow when I use rational numbers in s and t


#example functions
g = Polynomial([-Polynomial([-5//1,8,-5,2]),1], :y)
h = Polynomial([-Polynomial([-1//1,3,-3,1]),1],:y)
f1 = Polynomial([5, Polynomial([0,10]),1], :y)
f2 = Polynomial([5,10,1], :y)
k = Polynomial([5,10,7])


#Tests for PolynomialModulo and BivPolMod
k
km = PolynomialModulo(k,1)
km = PolynomialModulo(k, 2)
km = PolynomialModulo(k,3)
k


#f2
convert(typeof(g), f2)
#fm2 = BivPolMod(f2,1)
f2
f1
fm1 = BivPolMod(f1,1)
f1
fny = PolynomialModulo(f1,1)

#no gcdx, so hardcoding the values that gcdx of Nemo gave maybe
s = Polynomial(Polynomial(1//4), :y)
t = Polynomial(Polynomial(-1//4), :y)

#test newtoninversion
ginv = newtoninversion(g,Polynomials.degree(g))
ginv == 1
finv = newtoninversion(f2, Polynomials.degree(f2))
finv == Polynomial([-3,-10], :y)

#test rev
g
rev(g,5)
f1
#rev(f1,1)
#BoundsError: attempt to access 2-element Vector{Polynomial{Int64, :x}} at index [0:2]
rev(g, Polynomials.degree(g))
rev(f1, Polynomials.degree(f1))

f = Polynomial([Polynomial([5//1,-10]),6,1], :y)
f
g
h
s
t
@time (gstar, hstar) = henseltruncate(1,f,g,h,s,t)
@time (gstar, hstar) = henseltruncate(2,f,g,h,s,t)
@time (gstar, hstar) = henseltruncate(4,f,g,h,s,t)
@time (gstar, hstar) = henseltruncate(8,f,g,h,s,t)
#@time (gstar, hstar) = henseltruncate(128,f,g,h,s,t)

ginv = newtoninversion(g, Polynomials.degree(g))
finv = newtoninversion(f, Polynomials.degree(f))
