using AbstractAlgebra
using LinearAlgebra
using Plots
using Polynomials


PolynomialModulo(p,n)=Polynomial(p[0:n-1], Polynomials.indeterminate(p))
BivPolMod(q,n) = map(p->PolynomialModulo(p,n),q)

function newtoninversion(f, l)
	g = one(f)
	r = ceil(Int, log2(l))
	pow = 1
	print("\n", l, " ",  r)
	for i ∈ 1:r
		pow *=2
		f2 = PolynomialModulo(f, pow)
		g  = PolynomialModulo(2g-f2*g*g, pow) #le probleme est probablement dans les multiplications ici qui sont quadratiques
		print("-")
	end
	print("\"\n")
	g
end

function rev(p,d)
	t = zeros(eltype(p), d+1)
	pd = Polynomials.degree(p)
	t[d-pd+1:d+1] = reverse(p.coeffs)

	Polynomial(t,Polynomials.indeterminate(p))
end

function fastdivrem(a,b)
	#marche que pour les fonctions unitaires
	da = Polynomials.degree(a)
	db = Polynomials.degree(b)
	da<db && return (zero(a),a)
	m = da-db
	invrevb = newtoninversion(rev(b,db), m+1)
	reva = PolynomialModulo(rev(a,da), m+1)
	qstar = PolynomialModulo(reva*invrevb, m+1) #does this do mod y^(m+1) if reva and invrevb lose their y?
	q = rev(qstar,m)
	q, PolynomialModulo(a-b*q, db)
end


function divrem(a,b)
	#type conversions to assert that a and b have the same type of polynomial
	T = promote_type(typeof(a), typeof(b))
	a = convert(T, a)
	b = convert(T, b)

	#set initial r
	r = a

	da = Polynomials.degree(a)
	db = Polynomials.degree(b)

	u = newtoninversion(b,db)
	if da >= db #control that deg(a)>=deg(b)
		i = da-db
		q = Polynomial(zeros(i), Polynomials.indeterminate(a))
		q = convert(T, q)
		while i > 0
			dr = Polynomials.degree(r)
			if dr == db + i
				lcr = Polynomial(r.coeffs[dr+1], Polynomials.indeterminate(r.coeffs[dr+1])) #lc(r) is good
				lcrpoly = Polynomial([lcr,0], Polynomials.indeterminate(u))
				qi = u * lcrpoly
				q.coeffs[i] = qi.coeffs[1]
				#define y^i
				temp = zeros(i)
				temp[i] = 1.0
				ptemp = Polynomial(temp, Polynomials.indeterminate(a))
				ptemp = convert(T,ptemp)
				r = r - qi*ptemp*b
			else
				q.coeffs[i] = 0
			end		
			i = i-1
		end
	else
		print("error in division with remainder: polynomial 2 has bigger degree then polynomial 1\n")
		q = Polynomial([0],Polynomials.determinate(a))
	end 
	q, r
end

#divrem testing
f0 = Polynomial([1., 0, 2, 1])
f1 = Polynomial([0., 1, 3])
f2 = Polynomial([-1., 2, 2])
f = Polynomial([3*f0, f1, 3*f2], :y)

g = Polynomial([1.0,-1], :y)

#f*g
#g*f
#typeof(f)
#typeof(g)
#qfast, rfast = fastdivrem(f,g)
#f
#qfast*g + rfast

qslow, rslow = divrem(f,g)
newtoninversion(g, Polynomials.degree(g)+1)
g = convert(typeof(f), g)
qslow*g + rslow


####
#convert(Polynomial{Float64, :x}, 3.0)
#promote_type(typeof(f), typeof(g))
#promote_type(Polynomial{Float64, :x}, Polynomial{Float64, :y})

function hensel_step(deg,f,g,h,s,t)
	e = BivPolMod(f-g*h, 2deg)
	se = BivPolMod(s*e, 2deg)
	
	(q,r) = fastdivrem(se,h)
	gstar = BivPolMod(g+t*e+q*g, 2deg)
	hstar = BivPolMod(h+r, 2deg)


	b = BivPolMod(s*gstar+t*hstar-1, 2deg)
	sb = BivPolMod(s*b, 2deg)

	(c,d) = fastdivrem(sb,hstar)
	sstar = BivPolMod(s-d, 2deg)
	tb = BivPolMod(t*b, 2deg)
	cg = BivPolMod(c*gstar, 2deg)
	tstar = BivPolMod(t-tb-cg, 2deg)

	(2deg, gstar, hstar, sstar, tstar)
end

function hensel_step_truncate(deg,f,g,h,s,t)
	e = BivPolMod(f-g*h, 2deg)
	(q,r) = fastdivrem(s*e,h)
	n = Polynomials.degree(g)
	gstar = BivPolMod(g+t*e+q*g, 2deg)
	gstar = PolynomialModulo(gstar, n+1) #on tronque les zeros parce que il y a des annulations dans t*e + q*g
	hstar = BivPolMod(h+r, 2deg)

	b = BivPolMod(s*gstar+t*hstar-1, 2deg)
	sb = BivPolMod(s*b, 2deg)
	(c,d) = fastdivrem(sb,hstar)
	sstar = BivPolMod(s-d, 2deg)
	tstar = BivPolMod(t-t*b-c*gstar, 2deg)

	(2deg, gstar, hstar, sstar, tstar)
end

function hensel(l, f, g, h , s, t)
	deg = 1
	while deg < l
		(deg , g, h , s, t ) = hensel_step(deg, f , g, h, s, t)
	end
	(BivPolMod(g,l),BivPolMod(h,l),t) #@todo: As we only need s and t for the next iteration, the full hensel does not need to output
									  #s and t, different than the hensel_step. I don't understand yet why he put out t anyway
									  #most of the time he didn't use it anyway
end

deg, dg, dh, ds, dt, dev1, dev2 = 0, 0, 0, 0, 0, 0, 0

"""dg = degree.(g.coeffs)
dh = degree.(h.coeffs)
ds = degree.(s.coeffs)
dt = degree.(t.coeffs)
dev1 = BivPolMod(s*g+t*h,deg)
dev2 = BivPolMod(f-g*h,deg)
println("Modulo: $deg")
println("g:  $dg")
println("h:  $dh")
println("s:  $ds")
println("t:  $dt")
println("sg+th mod X^2^n : $dev1\n")
println("f-gh mod X^2^n : $dev2\n\n")"""

function henseltruncate(l, f, g, h , s, t)
	deg = 1
	n = Polynomials.degree(g)
	while deg < l
		(deg , g, h , s, t ) = hensel_step_truncate(deg, f , g, h, s, t)
		#println("sg+th mod X^2^n : $dev1\n")
		#println("f-gh mod X^2^n : $dev2\n\n")
	end
	(BivPolMod(g,l),BivPolMod(h,l))
end

"""t = PolynomialModulo(t, n)
dg = degree.(g.coeffs)
dh = degree.(h.coeffs)
ds = degree.(s.coeffs)
dt = degree.(t.coeffs)
dev1 = BivPolMod(s*g+t*h,deg)
dev2 = BivPolMod(f-g*h,deg)
println("Modulo: $deg")
println("g:  $dg")
println("h:  $dh")
println("s:  $ds")
println("t:  $dt")"""

function hensel_info(n,ga, ha; verbose = false, rtype = Float64, qtype = Rational{BigInt})
	gr = Polynomial{Rational{BigInt}, :Y}(ga)
	hr = Polynomial{Rational{BigInt}, :Y}(ha)
	
	if (Polynomials.degree(hr) == -1 || lu(hr) !=1 )  throw("h not monic") end

	(gcdr, sr ,tr) = eea(gr, hr)

	if gcdr != 1  throw("h and g not coprime: gcd = $gcdr") end

	(gr,hr,sr,tr) = Polynomial{Polynomial{qtype, :X}, :Y}.([gr,hr,sr,tr])

	X  = Polynomial{Polynomial{qtype, :X}, :Y}([[0,1]])
	Y  = Polynomial{Polynomial{qtype, :X}, :Y}([0,1])
	dg = Polynomials.degree(gr)
	dh = Polynomials.degree(hr)

	fr = gr*hr + X^(dg+dh) + X^3*Y^(dg+dh-1) + X

	(g,h,f,s,t) = map(Polynomial{Polynomial{rtype, :X}, :Y},[gr, hr, fr, sr, tr])
	grsa, hrsa, sra, tra = map(x -> fill(x, n+1), [gr, hr, sr, tr])
	gsa, hsa, sa, ta = map(x -> fill(x, n+1), [g, h, s, t])
	deg = 1
	for i ∈ 1:n 
		_,grsa[i+1],hrsa[i+1], sra[i+1], tra[i+1] = hensel_step(deg, fr, grsa[i],hrsa[i], sra[i], tra[i])
		_,gsa[i+1],hsa[i+1], sa[i+1], ta[i+1] = hensel_step_truncate(deg, f, gsa[i], hsa[i], sa[i], ta[i])
		deg *= 2
	end
	(f,grsa, hrsa, sra, tra, gsa, hsa, sa, ta)
end

function henselplot(l, f, g, h , s, t)
	deg = 1
	table = zeros(l)
	for i ∈ 1:l
		println(i)
		(deg , g, h , s, t ) = hensel_step(deg, f , g, h, s, t)
		table[i] = log10(BivNorm(BivPolMod(f-g*h,deg)))
	end
	plot(table)
end

function relativeerror(p1,p2)
	erel = 0
	eabs = 0
	for (i,q) ∈ enumerate(p2)
		for (j,b) ∈ enumerate(q)
			a  = p1[i-1][j-1]
			ecand = abs((a-b)/a)
			a!=0 ? erel<ecand && (erel = ecand) : (eabs = abs(a-b))
		end
	end
	erel,eabs
end


BivNorm(p) = LinearAlgebra.norm( map(LinearAlgebra.norm, p))
BivPolRat(p)= map(x -> map(Rational{BigInt},x),p)
BivPolFloat(p)= map(x -> map(Float64,x),p)
BivPolBigFloat(p)= map(x -> map(BigFloat,x),p)


##Exemple ###########################################################################################################




f0 = Polynomial([1., 0, 2, 1])
f1 = Polynomial([0., 1, 3])
f2 = Polynomial([-1., 2, 2])
f = Polynomial([3*f0, f1, 3*f2], :y)
#f = BivPolBigFloat(f)
fr = BivPolRat(f)

X = Polynomial([0.,1.])

g = Polynomial(3*[1.0,-1], :y)
#g = BivPolBigFloat(g)
g2 = Polynomial(3*[1.0,-(1.0-3e-5)], :y)
gr = BivPolRat(g)
g2r = BivPolRat(g2)


h = Polynomial([1.0, +1.0, 0], :y)
#h = BivPolBigFloat(h)
h2 = Polynomial([1.0, +1.0], :y)
hr = BivPolRat(h)
h2r = BivPolRat(h2)

BivPolMod(fr-gr*hr, 1)
BivPolMod(fr-g2r*h2r, 1)
BivPolMod(f - g*h, 1)
BivPolMod(f - g2*h2, 1)


s = Polynomial(Polynomial(1/6),:y)
#s = BivPolBigFloat(s)
s2 = Polynomial(Polynomial((0.5+1e-4)/3),:y)

sr = Polynomial(Polynomial(big(1)//6),:y)
s2r = Polynomial(Polynomial(big(5001)//30000),:y)

t = Polynomial(Polynomial(0.5),:y)
#t = BivPolBigFloat(t)
t2 = Polynomial(Polynomial(0.5-2e-4),:y)
tr = BivPolRat(t)
t2r = BivPolRat(t2)

s*g + t*h
typeof(1//2)


n = 128
(gs, hs,t) = hensel(n, f, g, h, s, t);0

plot(f1*f1-4*f0*f2, 0, 0.5)
(gs3, hs3) = henseltruncate(n, f, g, h, s, t)

(grs, hrs) = henseltruncate(n, fr, gr, hr, sr, tr);0#hensel originally, had third output tr before
print(grs, "\n")
print(hrs, "\n")
#(grs2, hrs2) = henseltruncate(n, fr, g2r, h2r, s2r, t2r) #hensel originally this takes pretty long

(gs2, hs2) = hensel(n, f, g2, h2, s2, t2);0 #hensel
(gs-gs2)(1)(1/10)
(hs-hs2)(1)(1/10)
BivNorm(gs-gs2)

(grs-gs3)(1)(1)
(hrs-hs3)(1)(1)

res  = BivPolMod(f-gs*hs,n)
res3 = BivPolMod(f-gs3*hs3,n)
resr = BivPolMod(fr-grs*hrs,n)

res3(1)(0.45)


res2 = BivPolMod(f-gs2*hs2, n)
res(1)(0)
res(1)(1/2)
res3(1)(0)
res3(1)(0.4)

res2(1)(0)
res2(1)(1/10)

bnr = BivNorm(res)
bnr3 = BivNorm(res3)
bnr2 = BivNorm(res2)

iter = 4

henselplot(iter, f, g, h, s, t)

for (i,x) ∈ enumerate(gs)
	println(i," ",x)
end
res2[0]
res2[1]

map(Rational,[1.,2,3])
BivNorm(res)

###EXEMPLE 2

f0 = Polynomial(big.([-1//1,1,0,2]))
f1 = Polynomial(big.([0//1,1//2,1//3]))
f2 = Polynomial(big.([0//1,0,2,2]))
f3 = Polynomial(big.([1//1,3,1//4,2//3]))

fr = Polynomial(3*[f0,f1,f2,f3], :y)
f = BivPolFloat(fr)
UN = one(fr)

gr = Polynomial(big.([3//1,3,3]), :y)
g = BivPolFloat(gr)

hr = Polynomial(big.([-1,1]), :y)
h = BivPolFloat(hr)

sr = Polynomial(big.([1//9]), :y)
s = BivPolFloat(sr)

tr = Polynomial(-big.([2//3, 1//3]), :y)
t = BivPolFloat(tr)

BivPolMod(fr-gr*hr, 1)
sr*gr+tr*hr


n = 128

(gs3, hs3) = henseltruncate(n, f, g, h, s, t);0 

(grs, hrs,tr) = henseltruncate(n, fr, gr, hr, sr, tr);0 #hensel

relativeerror(grs,gs3)

plot(0:0.01:1, x->log10(abs((grs-gs3)(1)(x))))
plot!(0:0.01:1, x->log10(abs((hrs-hs3)(1)(x))))

## TEST ------------------------------------------------------------------------------------------------

#leading coefficient
lu(f) = Polynomials.degree(f) ==-1 ? 1 : f[Polynomials.degree(f)]

function eea(f, g)
	ρ0 = lu(f)
	ρ1 = lu(g)
	r0 = f /ρ0
	r1 = g /ρ1
	s0 = 1/ρ0
	s1 = 0
	t0 = 0
	t1 = 1/ρ1
	while r1 != 0
		
		q1 = div(r0,r1)
		d = r0-q1*r1
		ρ2 = lu(d)
		r2 = d/ρ2
		s2 = (s0-q1*s1)/ρ2
		t2 = (t0 - q1*t1)/ρ2
		(ρ1, r0, r1, s0, s1, t0, t1) = (ρ2, r1, r2, s1, s2, t1, t2)
	end
	(r0,s0,t0)
end

function hensel_test(n,ga, ha; verbose = false, rtype = Float64, qtype = Rational{BigInt}) 
	gr = Polynomial{Rational{BigInt}, :Y}(ga)
	hr = Polynomial{Rational{BigInt}, :Y}(ha)
	
	if (Polynomials.degree(hr) == -1 || lu(hr) !=1 )  throw("h not monic") end

	(gcdr, sr ,tr) = eea(gr, hr)

	if gcdr != 1  throw("h and g not coprime: gcd = $gcdr") end

	(gr,hr,sr,tr) = Polynomial{Polynomial{qtype, :X}, :Y}.([gr,hr,sr,tr])

	X  = Polynomial{Polynomial{qtype, :X}, :Y}([[0,1]])
	Y  = Polynomial{Polynomial{qtype, :X}, :Y}([0,1])
	dg = Polynomials.degree(gr)
	dh = Polynomials.degree(hr)

	fr = gr*hr + X^(dg+dh) + X^3*Y^(dg+dh-1) + X

	(g,h,f,s,t) = map(Polynomial{Polynomial{rtype, :X}, :Y},[gr, hr, fr, sr, tr])
	@time (grs, hrs) = henseltruncate(n, fr, gr, hr, sr, tr) #hensel
	println("qdone")
	@time (gs, hs) = henseltruncate(n, f, g, h, s, t)
	(grs, hrs, gs, hs, fr)
end

function findR(f::Polynomial{Polynomial{T, X}, Y}) where {T,X,Y}
	Δ = Polynomial{Float64, X}(pdiscriminant(f))
	rΔ = Polynomials.roots(Δ)
	RΔs = map(abs, rΔ)
	RΔ,_ = findmin(RΔs)
	f0 = Polynomial{Float64, X}(lu(f))
	if Polynomials.degree(f0) == 0
		RΔ
	else
		r0 = Polynomials.roots(f0)
		R0s = map(abs, r0)
		R0,_ = findmin(R0s)
		min(R0,RΔ)
	end
end

function pdiscriminant(f::Polynomial{Polynomial{T, X}, Y}) where {T,X,Y}
	(Xpol, XX) = PolynomialRing(parent(T(0)), "X")
	(XYpol, YY) = PolynomialRing(Xpol, "Y")
	aafa = map( g-> g(XX), f.coeffs)
	aaf = XYpol(aafa)
	aaΔ = discriminant(aaf)
	Δ = Polynomial{T,X}(collect(coefficients(aaΔ)))
end

using BenchmarkTools


@time (grs, hrs, gs, hs, fr) = hensel_test(64,[5//7,0, 1,+3//5], [1//6,5//4, 1]; rtype = Float64);
# [1,1, 1//3,-2], [3,2,1]

@time (f, grsa, hrsa, sra, tra, gsa, hsa, sa, ta) = hensel_info(6,[5//7,0, 1,+3//5], [1//6,5//4, 1]; rtype = Float64);

deg = 1
for i ∈ 1:length(grsa)-1
	println(i,": ", BivPolMod(grsa[i+1]-grsa[i], deg)) #@todo: come back to this line
	deg *=2
end

deg = 1
for i ∈ 1:length(hrsa)-1
	println(i,": ", BivPolMod(hrsa[i+1]-hrsa[i], deg))
	deg *=2
end

function hensel_diff(f)
	deg = 1
	n = length(f)
	resa = fill(f[1], n-1)
	for i ∈ 1:n-1
		resa[i] = BivPolMod(f[i+1]-f[i], deg)
		deg *=2
	end
	resa
end


plot()
for i ∈ 0:(Polynomials.degree(grs))
	plot!(1:0.01:2, x->log10(abs((grs-gs)[i](x))), label = "δg_i") #add dollar sign sign in front of the last i
end
for i ∈ 0:Polynomials.degree(hrs)
	plot!(1:0.01:2, x->log10(abs((hrs-hs)[i](x))), label = "δh_i") #add dollar sign in front of the last i
end

#scatter!([-1.0798785289212511, 1.0798785289212511],[-15, -15], label = "R")
scatter!([1.0798785289212511],[ -15], label = "R")
relativeerror(grs,gs)
relativeerror(hrs,hs)



p = plot()

function ploterror!(p, f, fe, r, name; cutlast = false)
	for i ∈ 0:(Polynomials.degree(f)-cutlast*1)
		δf = (map(abs, (f-fe)[i]))
		plot!(p, r, x->log10(δf(x)), label = "δ$(name)_$i")
	end
end

function ploterrorval!(p, f, fe, r, name; cutlast = false)
	for i ∈ 1:1#(Polynomials.degree(f)-cutlast*1)
		δf = (map(abs, (f-fe)[i]))
		fa = (map(abs, (f)[i]))
		plot!(p, r, x->log10(δf(x)), label = "δ$(name)_$i")
		plot!(p, r, x->log10(fa(x)), label = "$(name)_$i")
		plot!(p, r, x->log10((δf(x))/(fa(x))), label = "δ$(name)/$(name)_$i")
	end
end

r = 0:0.01:3
ploterror!(p, grs, gs, r, 'g'; cutlast = true)
ploterror!(p, hrs, hs, r, 'h')
plot!()

function ploterror2(grs, hrs, gs, hs, f; cutlast = false)
	R = findR(f)
	r = range(0,3R,200)
	p = plot()
	ploterror!(p, grs, gs, r, 'g'; cutlast =cutlast)
	ploterror!(p, hrs, hs, r, 'h')
	scatter!([R],[ -15], label = "R")
	p
end

function ploterrorval2(grs, hrs, gs, hs, f; cutlast = false)
	R = findR(f)
	r = range(0,3R,200)
	p = plot()
	ploterrorval!(p, grs, gs, r, 'g'; cutlast = cutlast)
	#ploterrorval!(p, hrs, hs, r, 'h')
	scatter!([R],[ -15], label = "R")
	p
end

ploterror2(grs, hrs, gs, hs, fr)
ploterrorval2(grs, hrs, gs, hs, fr)

function ploterror3aux(f,fe,R)
	function auxaux(g)
		δf = map(abs, g)
		log10(δf(R))
	end
	map(auxaux, f-fe)
end

function ploterror3(fra,fa, f)
	fra = map(Polynomials.coeffs ,fra)
	fa = map(Polynomials.coeffs ,fa)
	R = findR(f)
	resa = map(x-> ploterror3aux(x..., R), zip(fra,fa))
	resb = reduce(vcat,transpose.(resa))
	plot(1:length(fra), resb)
end

#scatter!(p, [-1.0798785289212511, 1.0798785289212511],[-15, -15], label = "R")
scatter!([1.0798785289212511],[ -15], label = "R")

relativeerror(grs,gs)
relativeerror(hrs,hs)

function plotlograux(c)
	-log10(abs(c))
end

#plotlograux2(pol)=Polynomials.integrate(map(plotlograux, pol))
plotlograux2(pol)=map(plotlograux, pol)


function plotlogr!(pol,plot0)
	n = length(pol)
	scatter!(plot0,(plotlograux2(pol)).coeffs, smooth = true)
end

function plotlogr(pol)
	plot0 = plot()
	plotlogr!(pol, plot0)
end


function bivplotlogr!(pol, plot0)
	for (i,pol2) ∈ enumerate(pol)
		plotlogr!(pol2,plot0)
	end
	plot0
end

function bivplotlogr(pol)
	plot0 = plot()
	bivplotlogr!(pol,plot0)
end

plotlogr(grs[0])
bivplotlogr(grs)
bivplotlogr(hrs)
bivplotlogr(gs)
bivplotlogr(hs)
p = plot(); 0
for i ∈ 0:Polynomials.degree(grs)
	clog = plotlograux2(grs[i]).coeffs
	lc = length(clog)
	for i ∈	2:lc
		clog[i] = min(clog[i],clog[i-1] )
	end
	scatter!(p, clog, smooth=true)
end

plot!()

clog = plotlograux2(grs[0]).coeffs
lc = length(clog)
for i ∈	2:lc
	clog[i] = min(clog[i],clog[i-1] )
end
R = findR(fr)
R2 = 10^((clog[64]-clog[32])/32)
scatter!([R2],[ -15], label = "R2")
R2/R

X = Polynomials.variable(Polynomial())

