include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################
a0q = 2//3 -1//3*x_q +2//3*x_q^2-2*x_q^3+1//4*x_q^4+x_q^5-x_q^7+1//20 -1//7*x_q^9 -3*x_q^11 + x_q^12 -3*x_q^15-x_q^16-1//2*x_q^17-4//3*x_q^18
a1q = x_q -827//2*x_q^3 + 1//15*x_q^4 -10*x_q^5 - x_q^6 + 1//2*x_q^7 +1//4*x_q^9 -63*x_q^10 +x_q^11 -2*x_q^12 -5*x_q^13 +1//3*x_q^14 +2*x_q^15+13*x_q^16
a2q = -7//2 + 2*x_q -1//2*x_q^2 + 1//2*x_q^3 -1//11*x_q^5 - 1//6*x_q^6 -50*x_q^7+1//4*x_q^8 +1//2*x_q^9 +1//4*x_q^10 -10*x_q^11 -16*x_q^14 +1//6*x_q^16 +x_q^17 +32*x_q^18 -24*x_q^19
a3q = 13 -1//2*x_q +1//3*x_q^2 +2*x_q^3 -3*x_q^4 +15*x_q^6 -4*x_q^7+7//2*x_q^8-1//2*x_q^9 -2*x_q^10 +x_q^11 +2//3*x_q^12 +1//5*x_q^13 -x_q^14 -9//20*x_q^15 -3*x_q^16+2*x_q^17+x_q^18+x_q^19
a4q = -1 -1//11*x_q +6*x_q^2 + 2*x_q^3 +1//1220*x_q^4+19*x_q^5-2*x_q^6+x_q^7 -1//3*x_q^8 -x_q^9 -x_q^10 +x_q^11 +1//2*x_q^12 -1//27*x_q^13-x_q^14 -x_q^15+1//6*x_q^16
a5q = 1//3+1//35*x_q +1//3*x_q^2-x_q^3+1//8*x_q^4 +3//2*x_q^5 -x_q^6 + 15*x_q^7 -2*x_q^8 -x_q^9 -1//22*x_q^10-1//22*x_q^12
a6q = 3//4 + 1//54*x_q +2*x_q^3 +103*x_q^4+x_q^5-1//16*x_q^6 +x_q^7 -x_q^8-x_q^9 +x_q^10 -x_q^11 +3*x_q^12+x_q^13-3*x_q^14
a7q = -1 +x_q -1//2*x_q^2 +x_q^3-3//2*x_q^5-29*x_q^7-1//2*x_q^10 +x_q^11 -15//2*x_q^12-3*x_q^13-x_q^14 -17*x_q^15 +1//6*x_q^16+x_q^17 +x_q^18
a8q = 1//4 +48*x_q +5//6*x_q^2-9*x_q^3+1//13*x_q^5+1//2*x_q^6 +x_q^7 -22*x_q^8 +1//3*x_q^9-1//4*x_q^10 +2*x_q^11+6//11*x_q^12
aq = a0q + a1q*y_q + a2q*y_q^2 + a3q*y_q^3 + a4q*y_q^4 + a5q*y_q^5 +a6q*y_q^6 +a7q*y_q^7 +a8q*x_q^8
b3q = 1//5 -x_q^2 +1//3*x_q^3 -1//8*x_q^4 +x_q^5+1//18*x_q^6 +7//8*x_q^8+1//3*x_q^9 +x_q^10 -5//2*x_q^11 +x_q^12 +1//4*x_q^13 -x_q^14+5*x_q^15+5*x_q^16
b2q= 2 -x_q +7//2*x_q^3-x_q^4+1//3*x_q^6-2*x_q^7+1//8*x_q^8 +1//2*x_q^9-2//19*x_q^10+11*x_q^11 -1//5*x_q^14+1//3*x_q^15+x_q^16-1//13*x_q^17+13*x_q^18
b1q = 1//2 + 1//3*x_q +3//2*x_q^3 +x_q^4+x_q^5 -2*x_q^6+2*x_q^7 +8*x_q^8+13//4*x_q^9-7//4*x_q^12 +x_q^13 +9*x_q^14+1//3*x_q^15
b0q = -1//14 +3*x_q -3*x_q^2+x_q^3-1//44*x_q^4-5//6*x_q^5+3*x_q^6+5//2*x_q^7
bq = y_q^4 + b3q*y_q^3  + b2q*y_q^2 +b1q*y_q +b0q

ai = convert_polynomial(aq, [RBXY,RBX], RR)
ar = convert_polynomial(aq,[RXY,RX],RDF)

bi = convert_polynomial(bq, [RBXY,RBX], RR)
br = convert_polynomial(bq,[RXY,RX],RDF)

(qi, ri) = fastdivrem(ai,bi)
(qr, rr,binv) = fastdivrem(ar, br)

norm1error(qi)
norm1error(ri)

(qq,rq) = fastdivrem(aq,bq)
norm1error(qr,qq)
norm1error(rr,rq)

(qv,rv) = fastdivrem_validated_bivariate(ai,bi,20)
norm1error(qv)
norm1error(rv)
(qv,rv) = fastdivrem(ai,bi,20,validation = true)
norm1error(qv)
norm1error(rv)