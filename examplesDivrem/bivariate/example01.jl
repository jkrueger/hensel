include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

a1q = -22*x_q^6 + 1//12*x_q^5- 2*x_q^4 - 3//39*x_q^3 + 3//2*x_q +2//7
a2q = 1//2*x_q^8 - 1//5*x_q^7 -1//2*x_q^6 + 1//2*x_q^5+2*x_q^4+3//2*x_q^3 +1//2
a3q = x_q^6 +3*x_q^5+1//3*x_q^3+x_q-2
a4q = -3*x_q^8 -1//2*x_q^7 -x_q^6-1//3*x_q^4 + 11//4*x_q^3 + 2//21*x_q^2 -x_q + 1//6
a5q = 1//2*x_q^7-x_q^5-5*x_q^4-x_q^3 -1//4*x_q^2-8*x_q+16
aq = 1 + a1q*y_q + a2q*y_q^2 + a3q*y_q^3 + a4q*y_q^4 + a5q*y_q^5
bq = y_q^2 +(3*x_q+75//10)*y_q +(1//5*x_q^2+3//2*x_q +11//10)

ai = convert_polynomial(aq, [RBXY,RBX], RR)
ar = convert_polynomial(aq,[RXY,RX],RDF)

bi = convert_polynomial(bq, [RBXY,RBX], RR)
br = convert_polynomial(bq,[RXY,RX],RDF)

(qi, ri) = fastdivrem(ai,bi)
norm1error(qi)
norm1error(ri)


(qr, rr,binv) = fastdivrem(ar, br)

(qq,rq) = fastdivrem(aq,bq)
norm1error(qr,qq)
norm1error(rr,rq)
(qv, rv) = fastdivrem_validated_bivariate(ai,bi,9)
norm1error(qv)
norm1error(rv)
(qv,rv) = fastdivrem(ai,bi,9,validation = true)
norm1error(qv)
norm1error(rv)