include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")


###################################


a2q = ((1//3)*x_q +1)^5
b2q = (x_q + 7//5)^2

a2i = convert_polynomial(a2q,[RBX],RR)
a2r = convert_polynomial(a2q,[RX],RDF)

b2i = convert_polynomial(b2q, [RBX],RR)
b2r = convert_polynomial(b2q, [RX], RDF)

(qq,rq) = fastdivrem(a2q,b2q)
(qi,ri) = fastdivrem(a2i,b2i)
(qr,rr) = fastdivrem(a2r,b2r)

norm1error(qi)
norm1error(qr,qq)

norm1error(ri)
norm1error(rr,rq)

(qv,rv) = fastdivrem_validated(a2i,b2i)
norm1error(qv)
norm1error(rv)

(qv,rv) = fastdivrem(a2i,b2i,validation = true)
norm1error(qv)
norm1error(rv)

a2i
