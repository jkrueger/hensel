include("../../functions.jl")
using Nemo

RR = RealField()
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#########################################
a4 = (x_rb^5 + 1 )^17
b4 = (x_rb^14 -1)

(q,r) = fastdivrem(a4,b4)
interverror(q)
interverror(r)


a4r = (x^5 + 1)^17
b4r = (x^14 - 1)
(qr,rr) = fastdivrem(a4r,b4r)

a4q = (x_q^5 + 1)^17
b4q = (x_q^14 - 1)
(qq, rq) = fastdivrem(a4q, b4q)

qe = relerror(qr,qq)
re = relerror(rr, rq)

norm1error(q)
norm1error(qr,qq)

norm1error(r)
norm1error(rr,rq)