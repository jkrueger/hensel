include("../../functions.jl")
using Nemo

RR = RealField()
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

a6 = (RR(55)/RR(10)*x_rb +2)^663
b6 = (x_rb + RR(43)/RR(10))^223

a6r = (5.5*x +2)^663
b6r = (x + 4.3)^223

a6q = (55//10*x_q +2)^663
b6q = (x_q + 43//10)^223

(q,r) = fastdivrem(a6,b6)
interverror(q)
interverror(r)

plotAmpliErreur(q)
plotAmpliErreur(r)