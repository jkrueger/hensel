include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################
a11q = (11//27*x_q^30 + 9//7*x_q^2 -1//2)^3
b11q = (x_q^2 +7//5*x_q +13//10)^20

a11i = convert_polynomial(a11q,[RBX],RR)
a11r = convert_polynomial(a11q,[RX],RDF)

b11i = convert_polynomial(b11q, [RBX],RR)
b11r = convert_polynomial(b11q, [RX], RDF)

(qq,rq) = fastdivrem(a11q,b11q)
(qi,ri) = fastdivrem(a11i,b11i)
(qr,rr) = fastdivrem(a11r,b11r)

norm1error(qi)
norm1error(qr,qq)

norm1error(ri)
norm1error(rr,rq)
(qv,rv) = fastdivrem_validated(a11i,b11i)
norm1error(qv)
norm1error(rv)
(qv,rv) = fastdivrem(a11i,b11i,validation = true)
norm1error(qv)
norm1error(rv)
