include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

a9q = (32//10*x_q^30 + 7//5*x_q^2 -1//2)^3
b9q = x_q^20 + 2*x_q^19 + 19//10*x_q^18 + 11//10*x_q^17 + 53//10*x_q^16 + 18//10*x_q^15 + 48//10*x_q^14 + x_q^13 + 18//10*x_q^12 + 28//10*x_q^11 + 35//10*x_q^10 + 36//10*x_q^9 + 31//10*x_q^8 + 23//10*x_q^7 + 13//10*x_q^6 + 66//10*x_q^5 + 25//10*x_q^4 + 74//10*x_q^3 + 15//10*x_q^2 + 21//10*x_q + 13//10

a9i = convert_polynomial(a9q,[RBX],RR)
a9r = convert_polynomial(a9q,[RX],RDF)

b9i = convert_polynomial(b9q, [RBX],RR)
b9r = convert_polynomial(b9q, [RX], RDF)

(qq,rq) = fastdivrem(a9q,b9q)
(qi,ri) = fastdivrem(a9i,b9i)
(qr,rr) = fastdivrem(a9r,b9r)

norm1error(qi)
norm1error(qr,qq)

norm1error(ri)
norm1error(rr,rq)

(qv,rv) = fastdivrem_validated(a9i,b9i)
norm1error(qv)
norm1error(rv)
(qv,rv) = fastdivrem(a9i,b9i,validation = true)
norm1error(qv)
norm1error(rv)
