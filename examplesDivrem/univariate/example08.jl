include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

a8q = (32//10*x_q^30 + x_q^2 -1//2)^3
b8q = (x_q^2 +x_q +1)^20

a8i = convert_polynomial(a8q,[RBX],RR)
a8r = convert_polynomial(a8q,[RX],RDF)

b8i = convert_polynomial(b8q, [RBX],RR)
b8r = convert_polynomial(b8q, [RX], RDF)

(qq,rq) = fastdivrem(a8q,b8q)
(qi,ri) = fastdivrem(a8i,b8i)
(qr,rr) = fastdivrem(a8r,b8r)

norm1error(qi)
norm1error(qr,qq)

norm1error(ri)
norm1error(rr,rq)

(qv,rv) = fastdivrem_validated(a8i,b8i)
norm1error(qv)
norm1error(rv)
(qv,rv) = fastdivrem(a8i,b8i,validation = true)
norm1error(qv)
norm1error(rv)
