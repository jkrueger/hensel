include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

a5q = (1//5*x_q)^8
b5q = (x_q^2 +x_q -1)^3

a5i = convert_polynomial(a5q,[RBX],RR)
a5r = convert_polynomial(a5q,[RX],RDF)

b5i = convert_polynomial(b5q, [RBX],RR)
b5r = convert_polynomial(b5q, [RX], RDF)

(qq,rq) = fastdivrem(a5q,b5q)
(qi,ri) = fastdivrem(a5i,b5i)
(qr,rr) = fastdivrem(a5r,b5r)

norm1error(qi)
norm1error(qr,qq)

norm1error(ri)
norm1error(rr,rq)

(qv,rv) = fastdivrem_validated(a5i,b5i)
norm1error(qv)
norm1error(rv)

(qv,rv) = fastdivrem(a5i,b5i,validation = true)
norm1error(qv)
norm1error(rv)