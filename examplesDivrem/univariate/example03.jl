include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")


###########################################

a3q =  (21//10*x_q + 45//10)^15
b3q = (x_q + 89//10)^5

a3i = convert_polynomial(a3q,[RBX],RR)
a3r = convert_polynomial(a3q,[RX],RDF)

b3i = convert_polynomial(b3q, [RBX],RR)
b3r = convert_polynomial(b3q, [RX], RDF)

(qq,rq) = fastdivrem(a3q,b3q)
(qi,ri) = fastdivrem(a3i,b3i)
(qr,rr) = fastdivrem(a3r,b3r)

norm1error(qi)
norm1error(qr,qq)

norm1error(ri)
norm1error(rr,rq)

(qv,rv) = fastdivrem_validated(a3i,b3i)
norm1error(qv)
norm1error(rv)

(qv,rv) = fastdivrem(a3i,b3i,validation = true)
norm1error(qv)
norm1error(rv)