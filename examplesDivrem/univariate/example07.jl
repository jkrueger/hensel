include("../../functions.jl")

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

a7q = (32//10*x_q^30 + x_q^2 -1//2)^3
b7q = (x_q^2 +7//5*x_q +13//10)^20

a7i = convert_polynomial(a7q,[RBX],RR)
a7r = convert_polynomial(a7q,[RX],RDF)

b7i = convert_polynomial(b7q, [RBX],RR)
b7r = convert_polynomial(b7q, [RX], RDF)

(qq,rq) = fastdivrem(a7q,b7q)
(qi,ri) = fastdivrem(a7i,b7i)
(qr,rr) = fastdivrem(a7r,b7r)

norm1error(qi)
norm1error(qr,qq)

norm1error(ri)
norm1error(rr,rq)

@show b7r
(qv,rv) = fastdivrem_validated(a7i,b7i)
norm1error(qv)
norm1error(rv)

coeff(b7r,2)
Float64(coeff(b7i,2),RoundNearest)

(qv,rv) = fastdivrem(a7i,b7i,validation = true)
norm1error(qv)
norm1error(rv)