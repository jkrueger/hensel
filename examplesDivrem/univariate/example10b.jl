include("../../functions.jl")

RR = ArbField(53)
#RR = RealField()
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

#########################################

a10q = (32//10*x_q^30 + 9//7*x_q^2 -1//2)^3
b10q = 1000*x_q^40 + 2800*x_q^39 + 3984*x_q^38 + 3819*x_q^37 + 2764*x_q^36 + 1606*x_q^35 + 7777*x_q^34 + 3221*x_q^33 + 1162*x_q^32 + 3706*x_q^31 + 1054*x_q^30 + 2699*x_q^29 + 6261*x_q^28 + 1322*x_q^27 + 2553*x_q^26 + 4524*x_q^25 + 7373*x_q^24 + 1107*x_q^23 + 1536*x_q^22 + 1969*x_q^21 + 2335*x_q^20 + 2560*x_q^19 + 2596*x_q^18 + 2433*x_q^17 + 2106*x_q^16 + 1679*x_q^15 + 1232*x_q^14 + 8297*x_q^13 + 5107*x_q^12 + 2862*x_q^11 + 1453*x_q^10 + 6641*x_q^9 + 2708*x_q^8 + 9757*x_q^7 + 3062*x_q^6 + 8220*x_q^5 + 1839*x_q^4 + 3304*x_q^3 + 4480*x_q^2 + 4093*x_q + 1900

a10i = convert_polynomial(a10q,[RBX],RR)
a10r = convert_polynomial(a10q,[RX],RDF)

b10i = convert_polynomial(b10q, [RBX],RR)
b10r = convert_polynomial(b10q, [RX], RDF)

(qq,rq) = fastdivrem(a10q,b10q)
(qi,ri) = fastdivrem(a10i,b10i)
(qr,rr) = fastdivrem(a10r,b10r)

norm1error(qi)
norm1error(qr,qq)

norm1error(ri)
norm1error(rr,rq)

(qv,rv) = fastdivrem_validated(a10i,b10i)
norm1error(qv)
norm1error(rv)
(qv,rv) = fastdivrem(a10i,b10i,validation = true)
norm1error(qv)
norm1error(rv)