include("../../functions.jl")
using Nemo

RR = ArbField(53)
RBX, x_rb = PolynomialRing(RR, "x")
RBXY, y_rb = PolynomialRing(RBX, "y")

#definition of rational polynomial ring
QX, x_q = PolynomialRing(QQ, "x")
QXY, y_q = PolynomialRing(QX, "y")

#definition of real polynomial ring using Float64
#RDF for the Float64
RX, x = PolynomialRing(RDF, "x")
RXY, y = PolynomialRing(RX, "y")

#definition of real polynomial ring using BigFloat
FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
BFX, x_bf = PolynomialRing(FB, "x")
BFXY, y_bf = PolynomialRing(BFX, "y")

#definition of complex polynomial ring
CC = ComplexField()
CX, x_c = PolynomialRing(CC, "x")
CXY, y_c = PolynomialRing(CX, "y")

###############################


a1q = (2*x_q +4)^7
b1q = (x_q + 9)^4

a1i = convert_polynomial(a1q,[RBX],RR)
a1r = convert_polynomial(a1q,[RX],RDF)

b1i = convert_polynomial(b1q, [RBX],RR)
b1r = convert_polynomial(b1q, [RX], RDF)

(qq,rq) = fastdivrem(a1q,b1q)
(qi,ri) = fastdivrem(a1i,b1i)
(qr,rr) = fastdivrem(a1r,b1r)

norm1error(qi)
norm1error(qr,qq)

norm1error(ri)
norm1error(rr,rq)
