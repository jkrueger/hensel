using Nemo

"""
    error_with_R(p_exact::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, p_approx::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{T}}, f::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, k=1//2) where T <: AbstractFloat

Compute the error measure e = ∑|̄p_n-p_n|*(k*R)^n/∑|p_n|*(k*R)^n and R, with R the distance to the closest singularity in f 
"""
function error_with_R(p_exact::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, p_approx::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{T}}, f::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, k=1//2) where T <: AbstractFloat
    R = convergence_radius(f)
    e = error_measure1(p_exact,p_approx, k*R)
    (e,R)  
end

"""
    error_measure1(p_exact::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, p_approx::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{T}}, R=BigFloat(1)) where T <: AbstractFloat

∑|̄p_n-p_n|*R^n/∑|p_n|*R^n 
"""
#=function error_measure1(p_exact::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, p_approx::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{T}}, R=BigFloat(1)) where T <: AbstractFloat
    #print("\n \n")
    num = 0.0
    den = 0
    dp = degree(p_approx)
    for i ∈ 1:dp+1
        dpi = degree(p_approx.coeffs[i])
        for j ∈ 1:dpi+1
            num += (abs(coeff(p_approx.coeffs[i],j-1)-convert(Rational{BigInt}, coeff(p_exact.coeffs[i],j-1))))*R^(j-1)

            dentemp = convert(Rational{BigInt}, coeff(p_exact.coeffs[i],j-1))
            den += abs(dentemp)*R^(j-1)
        end
    end
    num/den
end=#

"""
    error_measure1(p_exact::PolyElem{T},p_approx::PolyElem{S}, R = BigFloat(1)) where {T<: PolyRingElem,S<: PolyRingElem}

∑|̄p_n-p_n|*R^n/∑|p_n|*R^n with y = 1
"""
function error_measure1(p_exact::PolyElem{T},p_approx::PolyElem{S}, R = BigFloat(1)) where {T<: PolyElem,S<: PolyElem} 
    #p_exact1 = deepcopy(p_exact)
    #p_approx1 = deepcopy(p_approx)
    #abs_coefficientwise!(p_exact1)
    #abs_coefficientwise!(p_approx1)
    #error_measure1(p_exact1(1),p_approx1(1),R)
    num = 0.0
    den = 0.0
    dp = degree(p_approx)
    for i ∈ 1:dp+1 
        dfi = degree(coeff(p_approx,i-1))
        for j ∈ 1:dfi 
            if T <: PolyElem{QQFieldElem}
                num += (abs(coeff(coeff(p_approx,i-1),j-1)-convert(Rational{BigInt}, coeff(coeff(p_exact,i-1),j-1))))*R^(i-1)
                den += abs(convert(Rational{BigInt}, coeff(coeff(p_exact,i-1),j-1)))*R^(i-1)
            elseif T<: PolyElem{AbstractFloat}
                num += (abs(coeff(coeff(p_approx,i-1),j-1)-convert(BigFloat, coeff(coeff(p_exact,i-1),j-1))))*R^(i-1)
                den += abs(convert(BigFloat, coeff(coeff(p_exact,i-1),j-1)))*R^(i-1)
            end            
        end
    end
    num/den
end

"""
    error_measure1(p_exact::PolyElem{T},p_approx::PolyElem{S}, R=BigFloat(1)) where {T <:Union{FieldElem, AbstractFloat},S <:Union{FieldElem, AbstractFloat}}

∑|̄p_n-p_n|*R^n/∑|p_n|*R^n 
"""
function error_measure1(p_exact::PolyElem{T},p_approx::PolyElem{S}, R=BigFloat(1)) where {T <:Union{FieldElem, AbstractFloat},S <:Union{FieldElem, AbstractFloat}}
    num = 0.0
    den  = 0.0
    dp = degree(p_approx)
    for i ∈ 1:dp+1
        #put the loop into the cases instead?
        if T == QQFieldElem
            num += (abs(coeff(p_approx,i-1)-convert(Rational{BigInt}, coeff(p_exact,i-1))))*R^(i-1)

            dentemp = convert(Rational{BigInt}, coeff(p_exact,i-1))
            den += abs(dentemp)*R^(i-1)
        elseif T <: AbstractFloat
            num += (abs(coeff(p_approx,i-1)-convert(BigFloat, coeff(p_exact,i-1))))*R^(i-1)

            dentemp = convert(BigFloat, coeff(p_exact,i-1))
            den += abs(dentemp)*R^(i-1)
        else 
            print("error_measure1: case for ", T, " not yet implemented\n")
        end
    end
    num/den
end

"""
    error_measure1(p_interv::PolyElem{T}, R = BigFloat(1)) where T <: PolyElem

∑|radius(p_i)|*R^i/∑|midpoint(p_i)|*R^i with y = 1
"""
function error_measure1(p_interv::PolyElem{T}, R = BigFloat(1)) where T <: PolyElem
    p_interv1 = deepcopy(p_interv)
    abs_coefficientwise!(p_interv1)
    #@show error_measure1(p_interv1(1),R)
    #@show error_measure1(p_interv(1),R)
    #@show p_interv(1)
    #@show p_interv1(1)
    error_measure1(p_interv1(1),R)
end

"""
    error_measure1(p_interv::PolyElem{T}, R=BigFloat(1)) where T

∑|radius(p_i)|*R^i/∑|midpoint(p_i)|*R^i with
"""
function error_measure1(p_interv::PolyElem{T}, R=BigFloat(1)) where T
    num = 0.0
    den = 0.0
    dp = degree(p_interv)
    #@show dp
    for i ∈ 1:dp+1
        #num += abs(convert(BigFloat, Nemo.radius(coeff(p_interv,i-1))))*R^(i-1)
        #den += abs(convert(BigFloat, coeff(p_interv,i-1)))*R^(i-1)

        #@show radius(coeff(p_interv,i-1))
        num += 2*Nemo.radius(coeff(p_interv,i-1))*R^(i-1)
        #@show num
        den += abs(coeff(p_interv,i-1))*R^(i-1)
        #@show den 
        #changed here 
        #Float64(radius(xx)/abs(xx), round=RoundUp) 
    end
    Float64(num/den, RoundUp)
end

"""
    interverror(f::PolyElem{T}) where T

∑radius(f_k)/∑midpoint(f_k) for polynomials with interval coefficients
"""
function interverror(f::PolyElem{T}) where T
    df = degree(f)
    sum = BigFloat(0.0)
    e = []

    if (T <: FieldElem)
        #univariate polynomial
        e = zeros(BigFloat, degree(f)+1)
        for i ∈ 1:df+1
            #radius
            #num = abs(convert(BigFloat, Nemo.radius(coeff(f,i-1))))
            num = 2*Nemo.radius(coeff(f,i-1))
    
            #midpoint
            #den = abs(convert(BigFloat, coeff(f,i-1)))
            den = abs( coeff(f,i-1))

            #avoid NaN for empty coefficients
            if (den == 0) & (num == 0)
                den = 1
            elseif (den  == 0)
                print( coeff(f, i-1), "\n")
            end

            e[i] = Float64(num/den, RoundUp)
            sum += Float64(num/den, RoundUp)
            #changed here
            #e[i] = Float64(radius(coeff(f,i-1))/abs(coeff(f,i-1)), RoundUp)
            #sum += radius(coeff(f,i-1))/abs(coeff(f,i-1))
        end
        sum = Float64(sum, RoundUp)
    elseif (T <: PolyElem)
        #bivariate polynomial
        e = zeros(BigFloat, degree(f)+1, maximum(map(i->degree(f.coeffs[i])+1,1:(degree(f)+1))))
        for i ∈ 1:df+1
            dfi = degree(f.coeffs[i])
            for j ∈ 1:dfi+1
                #radius
                num = 2*convert(BigFloat, Nemo.radius(coeff(f.coeffs[i],j-1)))
    
                #midpoint
                den = abs(convert(BigFloat, coeff(f.coeffs[i],j-1)))

                #avoid NaN for empty coefficients
                if den == 0 & num == 0
                    den = 1
                elseif den  == 0
                    print( coeff(f, i-1), "\n")
                end

                sum += num/den
                e[i,j] = num/den
            end
        end
    end

    (sum, e)
end

"""
    relerror(f_approx::PolyElem{T}, f_exact::PolyElem{S}) where {T,S}

∑|̃x_i-x_i|/|x_i|
"""
function relerror(f_approx::PolyElem{S}, f_exact::PolyElem{T}) where {S,T}
    df = degree(f_approx)
    e = 0.0
    for i ∈ 1:df+1
        if T <: QQFieldElem
            num = abs(coeff(f_approx, i-1)-convert(Rational{BigInt}, coeff(f_exact, i-1)))
            den = abs(convert(Rational{BigInt}, coeff(f_exact, i-1)))
        else
            num = abs(coeff(f_approx, i-1)-convert(BigFloat, coeff(f_exact, i-1)))
            den = abs(convert(BigFloat, coeff(f_exact, i-1)))
        end
        if (den == 0) & (num == 0)
            den = 1.0
        elseif (den == 0) 
            print("relerror: denominator = 0, nominator = ", num, "\n")           
        end
        e += num/den
    end
    e
end

"""
    norm1error(f_approx::PolyElem{S}, f_exact::PolyElem{T}) where {S <: PolyElem,T <:PolyElem}

∑|f_approx_ij - f_exact_ij|/∑|f_exact_ij|
"""
function norm1error(f_approx::PolyElem{S}, f_exact::PolyElem{T}) where {S <: PolyElem,T <:PolyElem}
    df = degree(f_approx)
    num = 0.0
    den = 0.0
    for i in 0:df
        dfi = degree(coeff(f_approx,i))
        for j in 0:dfi
            if T <: PolyElem{QQFieldElem}
                num += abs(coeff(coeff(f_approx,i),j)- convert(Rational{BigInt},coeff(coeff(f_exact,i),j)))
                den += abs(convert(Rational{BigInt},coeff(coeff(f_exact,i),j)))
            else
                num += abs(coeff(coeff(f_approx,i),j)- convert(BigFloat,coeff(coeff(f_exact,i),j)))
                den += abs(convert(BigFloat,coeff(coeff(f_exact,i),j)))
            end
        end        
    end
    num/den
end

"""
    norm1error(f_approx::PolyElem, f_exact::PolyElem)

||f_approx-f_exact||_1/||f_exact||_1
"""
function norm1error(f_approx::PolyElem{S}, f_exact::PolyElem{T}) where {S,T}
    df = degree(f_approx)
    num = 0.0
    den = 0.0
    for i ∈ 1:df+1
        if T <: QQFieldElem
            num += abs(coeff(f_approx, i-1)-convert(Rational{BigInt}, coeff(f_exact, i-1)))
            den += abs(convert(Rational{BigInt}, coeff(f_exact, i-1)))
        else
            num += abs(coeff(f_approx, i-1)-convert(BigFloat, coeff(f_exact, i-1)))
            den += abs(convert(BigFloat, coeff(f_exact, i-1)))
        end
    end
    num/den
end

"""
    norm1error(f::PolyElem{T}) where T <: FieldElem

∑|radius(f_i)|/∑|midpoint(f_i)|
"""
function norm1error(f::PolyElem{T}) where T <: FieldElem
    #@todo: can I add a type T for Balls? I don't think they are their own subtype tho, but only count as FieldElem
    df = degree(f)
    num = 0.0
    den = 0.0
    for i ∈ 1:df+1
        #radius
        #num += abs(convert(BigFloat, Nemo.radius(coeff(f,i-1))))
        
        #midpoint
        #den += abs(convert(BigFloat, coeff(f,i-1)))
        #changed here
        num += 2*Nemo.radius(coeff(f,i-1))
        den += abs(coeff(f,i-1))

    end
    #=@show num
    @show den 
    @show (midpoint(den))
    @show (radius(den))=#
    Float64(num/den, RoundUp)
end

"""
    norm1error(f::PolyElem{T}) where T <: PolyElem

∑|radius(f_i)|/∑|midpoint(f_i)| for |f(1,x)|
"""
function norm1error(f::PolyElem{T}) where T <: PolyElem
    f1 = deepcopy(f)
    abs_coefficientwise!(f1)
    norm1error(f1(1))
end

"""
    norm1error_rad_mid(r::PolyElem{T}, m::PolyElem{T}) where T <: Union{FieldElem,AbstractFloat}

∑|r_i|/∑|m_i|
"""
function norm1error_rad_mid(r::PolyElem{T}, m::PolyElem{T}) where T <: Union{FieldElem,AbstractFloat}
    d = degree(m)
    num = 0.0
    den = 0.0
    for i ∈ 1:d+1
        num += 2*abs(coeff(r,i-1))
        den += abs(coeff(m,i-1))
    end
    num/den
end

"""
    norm1error_rad_mid(r::PolyElem{T}, m::PolyElem{T}) where T <: PolyElem

∑|r_i|/∑|m_i| for |r(x,1)| and |m(x,1)|
"""
function norm1error_rad_mid(r::PolyElem{S}, m::PolyElem{T}) where {S <: PolyElem, T <: PolyElem}
    r1 = deepcopy(r)
    m1 = deepcopy(m)
    r1 = abs_coefficientwise!(r1)
    m1 = abs_coefficientwise!(m1)
    r1 = r1(1)
    m1 = m1(1)
    norm1error_rad_mid(r1,m1)    
end

"""
    norm1error_rad_mid(r::arb_poly, m::PolyElem{T}) where T<:AbstractFloat

∑|r_i|/∑|m_i| for |r(x,1)| and |m(x,1)|
"""
function norm1error_rad_mid(r::arb_poly, m::PolyElem{T}) where T<:AbstractFloat
    d = degree(m)
    num = 0.0
    den = 0.0
    for i ∈ 1:d+1
        num += 2*(Float64(abs(coeff(r,i-1)),RoundUp))
        den += abs(coeff(m,i-1))
    end
    Float64(num)/den
end
