using Nemo
using Plots

"""
    plot_error_along_R(pq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, pr::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{T}}, l =  "error along R", fq=pq, startIndex = 0, endIndex = 3, R = -1.0) where T <: AbstractFloat

Plot ∑|pr_n-pq_n|*r^n/∑|pq_n|*r^n for r ∈ [startIndex, endIndex] and show the convergence radius R.
"""
function plot_error_along_R(pq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, pr::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{T}}, l =  "error along R", fq=pq, startIndex = 0, endIndex = 3, R = -1.0) where T <: AbstractFloat
    #@todo thursday, sketch in notebook
    if R == -1
        (_,R) = error_with_R(pq,pr,fq)
    end
    rrange = range(startIndex,endIndex,200)
    plot(rrange, map(u->(error_measure1(pq,pr,u)), rrange), label = l)
    
    scatter!([R],[0], label = "R")
end

"""
    plot_error_along_R!(pq::PolyElem, pr::PolyElem, l = "error along R", startIndex=0, endIndex = 3)

Add ∑|pr_n-pq_n|*r^n/∑|pq_n|*r^n for r ∈ [startIndex, endIndex] to the current plot.
"""
function plot_error_along_R!(pq::PolyElem, pr::PolyElem, l = "error along R", startIndex=0, endIndex = 3)
    rrange = range(startIndex,endIndex, 200)
    plot!(rrange, map(u->(error_measure1(pq,pr,u)), rrange), label = l)
end

function plot_error_along_R!(p::PolyElem, l = "error along R", startIndex=0, endIndex = 3)
    rrange = range(startIndex,endIndex, 200)
    plot!(rrange, map(u->(error_measure1(p,u)), rrange), label = l)
end

"""
    hensel_test(f::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{T}}, g::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{T}}, h::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{T}}, fq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, gq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, hq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, l::Int, R = -1.0, r = -1.0) where T <: AbstractFloat

Compute the relative error of the Hensel lifting steps in Float64 for l steps, draw a plot of the errors of g*h, g, and h, after each step and return the error values at R/2 for each of them.
    # Arguments
- `f, g, h` polynomials in Float64 on which the Hensel lifting is to be done 
- `fq, gq, hq` the same polynomials in rational for comparison
- `l` number of lifting steps
"""
function hensel_test(f::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{T}}, g::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{T}}, h::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{T}}, fq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, gq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, hq::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, l::Int, R = -1.0, r = -1.0) where T <: AbstractFloat
    
    BivPolMod!(g,1)
    BivPolMod!(h,1)
    (_,s,t) = gcdx(g,h)
    (_, sq, tq) = gcdx(gq,hq)

    if R < 0
        R = convergence_radius(fq)
        @show R
        @show typeof(R)  
    end
    if r == -1
        r = R/2
    end

    deg = 1
    i = 1
    egh = zeros(BigFloat,l)
    eg = zeros(BigFloat, l)
    eh = zeros(BigFloat, l)
    d = zeros(Int, l)

    while deg < 2^l
        (_, g, h, s, t) = hensel_step_truncate(deg, f, g, h, s, t)
        (deg, gq, hq, sq, tq) = hensel_step_truncate(deg, fq, gq, hq, sq, tq)

        (egh[i]) = error_measure1(fq, g*h, r)
        (eg[i]) = error_measure1(gq, g, r)
        (eh[i]) = error_measure1(hq, h, r)

        display(scatter([R],[0], label = "R"))
        display(plot_error_along_R!(gq, g, string("g, n = ", deg), 0, 2R))
        display(plot_error_along_R!(hq, h, string("h, n = ", deg), 0, 2R))

        d[i] = deg
        i += 1
    end

    @show egh
    @show eg
    @show eh

    #display(plot(d,egh, label = "error of g*h at R/2"))
    display(plot(d,eg, label = "error of g at R/2"))
    display(plot!(d, eh, label = "error of h at R/2"))

    (eg, eh)
end

"""
    plotAmpliErreur(f::PolyElem)

TBW
"""
function plotAmpliErreur(f::PolyElem)
    (_, e) = interverror(f)
    c = zeros(Float64, degree(f)+1)
    for i ∈ eachindex(c)
        c[i] = abs(convert(Float64, coeff(f, i-1)))
    end
    plot(c, label = "coefficients", title = "")
    plot!(e, label = "relative error")    
end


"""
    plotAmpliRadius(f::PolyElem)

TBW
"""
function plotAmpliRadius(f::PolyElem)
    c = zeros(Float64, degree(f)+1)
    r = zeros(Float64, degree(f)+1)
    for i ∈ eachindex(c)
        c[i] = abs(convert(Float64, coeff(f, i-1)))
        r[i] = abs(convert(Float64,radius(coeff(f, i-1))))
    end
    plot(c, label = "coefficients", title = "")
    plot!(r, label = "interval radius")    
end

function plotAmpliRadiusError(f::PolyElem)
    c = zeros(Float64, degree(f)+1)
    r = zeros(Float64, degree(f)+1)
    (_, e) = interverror(f)
    for i ∈ eachindex(c)
        c[i] = abs(convert(Float64, coeff(f, i-1)))
        r[i] = abs(convert(Float64,radius(coeff(f, i-1))))
    end
    display(plot(log.(c), label = "coefficients", title = ""))
    display(plot!(log.(r), label = "interval radius"))
    display(plot!(log.(e), label = "relative error"))
end