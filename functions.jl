using Nemo
using Plots
include("errormeasures.jl")
include("testfunctions.jl")
include("practicalfunctions.jl")

#code based on Léo's code Hensel.jl

"""
    BivPolMod(q::PolyElem, n::Int)

q mod x^n for bivariate polynomials
"""
function BivPolMod!(q::PolyElem, n::Int)
    dq = degree(q)
    for i ∈ 1:dq+1
        q.coeffs[i] = truncate(q.coeffs[i],n)
    end
    q    
end

"""
    newtoninversion(f::PolyElem, l::Int)

Invert the polynomial f through a Newton iteration such that fg=1 mod x^l
works for f(0) =/= 0
"""
function newtoninversion(f::PolyElem, l::Int)
    g = parent(f)(1/coeff(f,0))
    r = ceil(Int, log2(l))
    pow = 1
    for i ∈ 1:r
        pow *= 2
        g = 2g-mullow(f,g*g,pow)
        g = truncate(g,pow)
    end
    g = truncate(g,l)
    g
end

"""
    newtoninversion(f::PolyElem, l::Int)

Invert the polynomial f through a Newton iteration such that fg=1 mod y^l mod x^{k}
works for f(0) a nonzero constant
"""
function newtoninversion(f::PolyElem, l::Int, k::Int)
    g = parent(f)(1/coeff(f,0))
    r = ceil(Int, log2(l))
    pow = 1
    for i ∈ 1:r
        pow *= 2
        g = 2g-mullow(f,g*g,pow)
        g = truncate(g,pow)
        BivPolMod!(g,k)
    end
    g = truncate(g,l)
    g
end

"""
    newtoninversion(f::PolyElem, l::Int; validation::Bool=false)

Invert the polynomial f through a Newton iteration such that fg=1 mod y^l mod x^{k}
works for f(0) a nonzero constant
if validation = true, validate and bound the numeric result after computation. 
"""
function newtoninversion(f::PolyElem, l::Int; validation::Bool=false)
    if (validation)
        ff = convert_balls_to_Float(f)
    else
        ff = f
    end
    g = parent(ff)(1/coeff(ff,0))
    r = ceil(Int, log2(l))
    pow = 1
    for i ∈ 1:r
        pow *= 2
        g = 2g-mullow(ff,g*g,pow)
        g = truncate(g,pow)
    end
    g = truncate(g,l)
    if (validation)
        (r,_,_) = validate_a_posteriori_inverse(g,f)
        g = convert_polynomial_to_intervals(g,r)
    end
    g
end

"""
    newtoninversion(f::PolyElem, l::Int, k::Int; validation::Bool =false)

Invert the polynomial f through a Newton iteration such that fg=1 mod y^l mod x^{k}
works for f(0) a nonzero constant
if validation = true, validate and bound the numeric result after computation. 
"""
function newtoninversion(f::PolyElem, l::Int, k::Int; validation::Bool =false)
    if (validation)
        ff = convert_balls_to_Float(f)
    else
        ff = f
    end
    g = parent(ff)(1/coeff(ff,0))
    r = ceil(Int, log2(l))
    pow = 1
    for i ∈ 1:r
        pow *= 2
        g = 2g-mullow(ff,g*g,pow)
        g = truncate(g,pow)
        BivPolMod!(g,k)
    end
    g = truncate(g,l)
    if validation
        (r,_,_) = validate_a_posteriori_inverse_bivariate(g,f,k)
        g = convert_polynomial_to_intervals(g,r)
    end
    g
end

"""
    rev(f::PolyElem)

Reverse the coefficients of a polynomial f.
"""
function rev(f::PolyElem)
    rev(f,length(f))    
end

"""
    rev(f::PolyElem, len::Int)

Reverse the polynomial f.
Based on the reverse function from Nemo/AbstractAlgebra,
but not normalised, since normalisation is not implemented
for all PolyElem subtypes
"""
function rev(f::PolyElem, len::Int)
    len < 0 && throw(DomainError(len, "len must be >= 0"))
    r = parent(f)()
    fit!(r, len)
    for i = 1:len
        z = setcoeff!(r, i-1, coeff(f, len -i))
    end
    return r
end

"""
    fastdivrem(a::PolyElem, b::PolyElem)

Divide polynomial a by polynomial b with remainder and return q and r such that a = q*b + r.
b has to be monic.
"""
function fastdivrem(a::PolyElem, b::PolyElem)
    da = degree(a)
    db = degree(b)
    da<db && return (zero(a),a)
    m = da-db
    invrevb = newtoninversion(rev(b, length(b)), m+1)
    #@show invrevb
    reva = deepcopy(rev(a, length(a))) #need to make a copy otherwise the following
                                #truncation changes the a as well
                                #reverse seems to work directly on the coefficients
    reva = truncate(reva, m+1)
    qstar = reva*invrevb
    qstar = truncate(qstar, m+1)
    #@show qstar
    q = rev(qstar, length(qstar))
    r = a -b*q
    #@show a
    #@show b
    #@show r
    r = truncate(r, db)
    #@show r
    q, r, invrevb
end

"""
    fastdivrem(a::PolyElem, b::PolyElem; validation::Bool = false)

Divide polynomial a by polynomial b with remainder and return q and r such that a = q*b + r.
b has to be monic.
If validation = true, validate and bound the numeric result of the substeps.
"""
function fastdivrem(a::PolyElem, b::PolyElem; validation::Bool = false)
    da = degree(a)
    db = degree(b)
    da<db && return (zero(a),a)
    m = da-db
    invrevb = newtoninversion(rev(b, length(b)), m+1, validation = validation)
    reva = deepcopy(rev(a, length(a))) #need to make a copy otherwise the following
                                #truncation changes the a as well
                                #reverse seems to work directly on the coefficients
    reva = truncate(reva, m+1)
    qstar = reva*invrevb
    qstar = truncate(qstar, m+1)
    q = rev(qstar, length(qstar))
    r = a -b*q
    r = truncate(r, db)
    q, r, invrevb
end

"""
    fastdivrem(a::PolyElem, b::PolyElem, k::Int; validation::Bool = false)

Divide polynomial a by polynomial b with remainder and return q and r such that a = q*b + r.
b has to be monic.
If validation = true, validate and bound the numeric result of the substeps.
"""
function fastdivrem(a::PolyElem, b::PolyElem, k::Int; validation::Bool = false)
    da = degree(a)
    db = degree(b)
    da<db && return (zero(a),a)
    m = da-db
    invrevb = newtoninversion(rev(b,length(b)),m+1,k,validation = validation)
    q_star = mullow(rev(a,length(a)),invrevb,m+1)
    BivPolMod!(q_star,k)
    q = rev(q_star,m+1)
    r = a-b*q
    r = truncate(r,db)
    BivPolMod!(r,k)
    (q,r)
end

"""
    fastdivrem_validated(ai::arb_poly, bi::arb_poly)

Divide polynomial a by polynomial b with remainder and return q and r such that a = q*b + r.
b has to be monic.
If validation = true, validate and bound the numeric result of the substeps.
"""
function fastdivrem_validated(ai::arb_poly, bi::arb_poly)
    da = degree(ai)
    db = degree(bi)
    da<db && return (zero(ai),ai)
    m = da-db
    #b = convert_balls_to_Float(bi)
    #@show b
    #invrevb = newtoninversion(rev(b,length(b)),m+1)
    #@show invrevb
    #(rb,_,_) = validate_a_posteriori_inverse(invrevb,rev(bi,length(bi)))
    #invrevbi = convert_polynomial_to_intervals(invrevb,rb)
    invrevbi = newtoninversion(rev(bi,length(bi)),m+1,validation = true)
    q_star = mullow(rev(ai,length(ai)),invrevbi,m+1)
    #@show q_star
    q = rev(q_star,m+1)
    r = ai - bi*q
    r = truncate(r,db)
    (q,r)
end

"""
    fastdivrem_validated_bivariate(ai::PolyElem{T}, bi::PolyElem{T}, k::Int) where T <: arb_poly

Divide polynomial a by polynomial b with remainder and return q and r such that a = q*b + r.
b has to be monic.
If validation = true, validate and bound the numeric result of the substeps.
"""
function fastdivrem_validated_bivariate(ai::PolyElem{T}, bi::PolyElem{T}, k::Int) where T <: arb_poly
    da = degree(ai)
    db = degree(bi)
    da<db && return (zero(ai),ai)
    m = da-db
    #b = convert_balls_to_Float(bi)
    #invrevb = newtoninversion(rev(b,length(b)),m+1,k)
    #@show invrevb
    #@show mullow(invrevb,rev(b,db+1),m+1)
    #(rb,t,_) = validate_a_posteriori_inverse_bivariate(invrevb,rev(bi,length(bi)),k)
    #@show t
    #invrevbi = convert_polynomial_to_intervals(invrevb,rb)
    invrevbi = newtoninversion(rev(bi,length(bi)),m+1,k,validation = true)
    q_star = mullow(rev(ai,length(ai)),invrevbi,m+1)
    BivPolMod!(q_star,k)
    q = rev(q_star,m+1)
    r = ai-bi*q
    truncate(r,db)
    BivPolMod!(r,k)
    (q,r)
end


"""
    hensel_step_truncate(deg::Int, f::PolyElem, g::PolyElem, h::PolyElem, s::PolyElem, t::PolyElem)

A single Hensel lifting step.
"""
function hensel_step_truncate(deg::Int, f::PolyElem, g::PolyElem, h::PolyElem, s::PolyElem, t::PolyElem; validation::Bool = false)
    print(deg, "-----------------\n")

    e = f-g*h
    BivPolMod!(e, 2deg)
    (q,r) = fastdivrem(s*e,h,2deg,validation = validation)
    n = degree(g)
    gstar = g+t*e+q*g
    BivPolMod!(gstar, 2deg)
    gstar = truncate(gstar, n+1)
    @show gstar
    #@show r
    hstar = h+r
    BivPolMod!(hstar, 2deg)
    
    b = s*gstar + t*hstar-1
    BivPolMod!(b, 2deg)
    sb = s*b
    BivPolMod!(sb, 2deg)
    #@show hstar
    (c,d) = fastdivrem(sb, hstar, 2deg, validation = validation)
    sstar = s-d
    BivPolMod!(sstar, 2deg)
    tstar = t-t*b-c*gstar
    BivPolMod!(tstar, 2deg)

    (2deg, gstar, hstar, sstar, tstar)
end


"""
    henseltruncate(l::Int, f::PolyElem, g::PolyElem, h::PolyElem, s::PolyElem, t::PolyElem)

Do a Hensel lifting on the polynomials g and h  such that f ≡ g* h* mod x^l

# Arguments
- `g, h` need to be given such that f≡gh mod x
- `s, t` need to be given such that sg + th ≡ 1 mod x
- `h` is monic
- `l` power of two
"""
function henseltruncate(l::Int, f::PolyElem, g::PolyElem, h::PolyElem, s::PolyElem, t::PolyElem; validation::Bool = false)
    deg = 1
    while deg < l
        (deg, g, h, s, t) = hensel_step_truncate(deg, f, g, h, s, t, validation = validation)
    end
    BivPolMod!(g,l)
    BivPolMod!(h,l)
    (g,h,s,t)
end

"""
    henseltruncate(l::Int, f::PolyElem, g::PolyElem, h::PolyElem; validation::Bool = false)

Do a Hensel lifting on the polynomials g and h  such that f ≡ g* h* mod x^l

# Arguments
- `g, h` need to be given such that f≡gh mod x
- `h` is monic
- `l` power of two
If validation = true, validate and bound the numeric result of the substeps.
"""
function henseltruncate(l::Int, f::PolyElem, g::PolyElem, h::PolyElem; validation::Bool = false)
    deg = 1
    (_,s,t) = gcdx(g,h)
    while deg < l
        (deg, g, h, s, t) = hensel_step_truncate(deg, f, g, h, s, t, validation = validation)
    end
    BivPolMod!(g,l)
    BivPolMod!(h,l)
    (g,h,s,t)
end

"""
    hensel_validate(l::Int, f::PolyElem, g::PolyElem, h::PolyElem; ϵ=1e-10, imax = 20)

Compute Hensel in Float64 polynomials and validate and bound the result a posteriori
"""
function hensel_validate(l::Int, f::PolyElem, g::PolyElem, h::PolyElem; ϵ=1e-10, imax = 20)
    dg = degree(g)
    dh = degree(h)
    df = degree(f)
    
    @show dg
    @show dh

    #convert input polynomials to real polynomials in Float64
    gr = convert_balls_to_Float(g)
    hr = convert_balls_to_Float(h)
    fr = convert_balls_to_Float(f)
    #compute Hensel with Float64
    (g,h,s,t) = henseltruncate(l,fr,gr,hr)
    #convert resulting polynomials to thin intervals
    ds = degree(s)
    dt = degree(t)
    g = convert_float_to_balls(g)
    h = convert_float_to_balls(h)
    s = convert_float_to_balls(s)
    t = convert_float_to_balls(t)

    #compute η
    e = g*h -f
    (q,r) = fastdivrem(s*e,h,l,validation = true)
    δ_g = t*e +q*g
    δ_g = trunc(δ_g,dg+1,l)
    abs_coefficientwise!(δ_g)
    δ_g = mag_coefficientwise(δ_g)
    @show δ_g

    δ_h = r
    δ_h = trunc(δ_h,dh+1,l)
    abs_coefficientwise!(δ_h)
    δ_h = mag_coefficientwise(δ_h)
    @show δ_h

    δmin_g = nonzeroMin(δ_g)
    δmin_h = nonzeroMin(δ_h)
    η_g = δ_g
    η_h = δ_h
    replace_zeros!(η_g,δmin_g,dg,l)
    replace_zeros!(η_h,δmin_h,dh,l)
    η_g = ϵ*η_g
    η_h = ϵ*η_h
    @show degree(η_g)
    @show degree(η_h)

    for i in 0:imax
        e = g*h - f
        BivPolMod!(e,l)
        (q,r) = fastdivrem(s*e,h,l,validation = true)
        gg = g + t*e + q*g
        gg = trunc(gg,dg+1,l)
        hh = h + r
        hh = trunc(hh,dh+1,l)
        #@show isless_coefficientwise(extract_radius(hh)-extract_radius(h),η_h)
        #@show extract_radius(hh)-extract_radius(h)
        #@show η_h
        #@show isless_coefficientwise(extract_radius(gg)-extract_radius(g),η_g)
        if (isless_coefficientwise(extract_radius(hh)-extract_radius(h),η_h))&&(isless_coefficientwise(extract_radius(gg)-extract_radius(g),η_g)) 
            return (g,h)            
        end
        g = gg
        h = hh        

        #update the cofactors
        
        b = s*g +t*h -1
        BivPolMod!(b,l)
        (c,d) = fastdivrem(s*b,h,l,validation = true)
        s = s-d
        s = trunc(s,ds+1,l)
        t = t-t*b-c*g
        t = trunc(t,dt+1,l)
    end
    print("no convergence\n")
    (g,h)
end

"""
    subtract(p::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, q::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}})

Subtract a real polynomial from a rational one.
Result is in BigFloat (type: Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}}) to accomodate for QQPolyRingElem allowing
bigger integers than Int64 and Float64 know how to handle
@todo: currently does not handle well if the first index of any of the coefficient polynomials is 0 after subtraction
"""
function subtract(p::Nemo.AbstractAlgebra.Generic.Poly{QQPolyRingElem}, q::Nemo.AbstractAlgebra.Generic.Poly{Nemo.AbstractAlgebra.Generic.Poly{Float64}})
    dp = degree(p)
    dq = degree(q)
    dr = max(dp,dq)
    r = BFXY()
    v = collect(coefficients(p))
    w = collect(coefficients(q))
    for i ∈ 1:dr+1
        n = max(degree(v[i]), degree(w[i]))
        set_coefficient!(r,i-1,1)
        for j ∈ n+1:-1:1
            co = convert(Rational{BigInt}, coeff(p.coeffs[i],j))-coeff(q.coeffs[i],j) #this gives a conversion error if the 
            print(i-1, "\t", j-1, "\t", convert(Rational{BigInt}, coeff(p.coeffs[i],j-1)), "\t", coeff(q.coeffs[i],j-1), "\n")
            #print("co: ", co, "\n")
            #print(r.coeffs[i], "\n")
            set_coefficient!(r.coeffs[i], j, co) #doesn't work to set j=1 to be 0
            #print(i, "\t", j, "\t", coeff(r.coeffs[i],j-1), "\n")
        end
    end
    r
end

"""
    convergence_radius(f::PolyElem)

TBW
"""
function convergence_radius(f::PolyElem)
    dis = discriminant(f)

    if degree(dis) == 0
        #print("degree of the discriminant is 0")
    elseif degree(dis) == 1
        r = roots(dis, QQ)
    else
        d = gcd(dis, derivative(dis))
        if d == 1
            r = roots(dis,QQ) 
        else
            d = divexact(dis,d)
            r = roots(d, QQ) 
        end        
    end
    #@show r
    R = minimum(abs.(r))
    if (R == 0)
        #@todo take the next smallest r
    end
    convert(Rational{BigInt}, R) #the conversion doesn't work anymore for some reason
    #@show R
    #print(typeof(R))
    R
end


"""
    abso!(f::PolyElem{T}) where T

Compute the coefficientwise absolute of a Polynomial f
"""
function abso!(f::PolyElem{T}) where T
    df = degree(f)
    if T <: Union{FieldElem, AbstractFloat}
        for i ∈1:df+1
            set_coefficient!(f, i-1, abs(coeff(f,i-1)))
        end
    elseif T <: PolyElem
        for i ∈1:df+1
            dfi = degree(f.coeffs)
            for j ∈ 1:dfi+1
                set_coefficient!(f.coeffs[i], j-1, abs(coeff(f.coeffs[i],j-1)))
            end
            #or just abso!(f.coeffs[i]) ?
        end
    end
    f
end

"""
    conv(f::PolyElem{T}, k::Int) where T

Construct the convolution matrix C_k(f).
"""
function conv(f::PolyElem{T}, k::Int) where T 
    df = degree(f)
    C = zeros(T, df+k, k+1)
    for i ∈ 1:df+k
        for j ∈ 1:k+1
            s = i-j+1
            if (s > 0) & (s < df+1)
                C[i,j] = coeff(f,i-j+1)
            end
        end
    end
    C
end


"""
    validate_a_posteriori_inverse(q::PolyElem{T}, b::PolyElem{S}; ϵ = 1e-10, tmax = 20, trunc = -1) where {T,S}

A posteriori validation of a numerical solution q to qb-1 = 0 mod x^(degree(b)+1)
Returns coefficientwise error bounds for q in the form of a polynomial.
# Arguments 
- `q` approximate solution to qb-1 = 0 mod x^(degree(b)+1), float-valued polynomial
- `b` interval valued polynomial 
"""
function validate_a_posteriori_inverse(q::PolyElem{T}, b::PolyElem{S}; ϵ = 1e-10, tmax = 20, trunc = -1) where {T,S}#{T <: AbstractFloat,S <: FieldElem}
    k = degree(q)

    #use rigorous versions of the polynomials q and b for further computations
    q_rig = convert_float_to_balls(q)
    #b_rig = convert_float_to_balls(b)
    b_rig = b

    #compute η and δ
    #where |N(x)-N(x')| <= η |x-x'|
    #and δ >= |N(x)-x|
    η = 1 - mullow(q_rig, b_rig, k+1)
    δ = mullow(q_rig,η,k+1)

    η = mag_coefficientwise(η)
    δ = mag_coefficientwise(δ)
    #@show η
    #@show δ

    #truncate in the second variable for bivariate functions
    #if T <: PolyElem 
    #    BivPolMod!(η, trunc)
    #    BivPolMod!(δ, trunc)
    #end

    #replace zero entries in δ by the smallest nonzero entry
    δmin = nonzeroMin(δ)
    #@show δmin
    e = parent(δ)(0)
    for i ∈ 1:k+1
        if T <: PolyElem
            for j ∈ 1:trunc
                if (coeff(coeff(δ,i-1),j-1)== 0)
                    set_coefficient!(e.coeffs[i],j-1, δmin)
                else
                    set_coefficient!(e.coeffs[i], j-1, coeff(coeff(δ,i-1),j-1))
                end
            end
        else
            if (coeff(δ, i-1)== 0)
                set_coefficient!(e,i-1, δmin)
            else
                set_coefficient!(e,i-1, coeff(δ,i-1))            
            end
        end
    end
    #@show δ

    #break condition and iterator
    e = ϵ * e 
    δ_plus = mag_coefficientwise(δ + e)

    r = parent(η)(0)
    for t ∈ 0:tmax
        rr = mullow(η, r, k+1) + δ_plus
        #@show (η*r)
        rr = mag_coefficientwise(rr)
        #if T <: PolyElem
        #    BivPolMod!(rr, trunc)            
        #end

        diff = rr - r 
        
        #@show t
        #@show norm1error_rad_mid(r,q)
        #@show r
        #@show rr

        abs_coefficientwise!(diff)
        if isless_coefficientwise(diff,e)
            r = convert_balls_to_upperBound_Float(r)
            return (r,t, "")
        end
        r = rr
    end
    r = convert_balls_to_upperBound_Float(r)
    print("no convergence in ", tmax, " steps, last r = ", r, "\n")
    (r, tmax, string("no convergence in ", tmax, " steps, last r = ", r))
end

"""
    validate_a_posteriori_inverse_bivariate(q::PolyElem{T}, b::PolyElem{S}, trunc::Int; ϵ = 1e-10, tmax = 20) where {T <: PolyElem, S<: PolyElem}

A posteriori validation of a numerical solution q to qb-1 = 0 mod y^(degree(b)+1)
Returns coefficientwise error bounds for q in the form of a polynomial.
# Arguments 
- `q` approximate solution to qb-1 = 0 mod y^(degree(b)+1), float-valued polynomial
- `b` interval valued polynomial 
"""
function validate_a_posteriori_inverse_bivariate(q::PolyElem{T}, b::PolyElem{S}, trunc::Int; ϵ = 1e-10, tmax = 20) where {T <: PolyElem, S<: PolyElem}
    db = degree(b)
    q_rig = convert_float_to_balls(q)
    #@show q_rig
    #@show b
    
    η = 1-mullow(q_rig,b,db+1)
    BivPolMod!(η,trunc)

    δ = mullow(q_rig,η,db+1)
    BivPolMod!(δ, trunc)

    η = mag_coefficientwise(η)
    δ = mag_coefficientwise(δ)
    #@show η
    #@show δ

    if δ == 0
        return (parent(q)(0),0,"exact solution")        
    end
    #e
    #option 1
    δmin = nonzeroMin(δ)
    e = δ
    for i ∈ 1:db+1
        for j ∈ 1:trunc
            if (coeff(coeff(e,i-1),j-1)== 0)
                set_coefficient!(e.coeffs[i],j-1, δmin)
            end
        end
    end

    e = ϵ*e
    δ_plus = mag_coefficientwise(δ + e)

    r = parent(q_rig)(0)
    for t in 0:tmax
        rr = mullow(η,r,db+1) +δ_plus
        BivPolMod!(rr, trunc)
        rr = mag_coefficientwise(rr)

        diff = rr - r
        if isless_coefficientwise(diff,e)
            r = convert_balls_to_upperBound_Float(r)
            return (r, t, "")
        end
        r = rr
    end
    r = convert_balls_to_upperBound_Float(r)
    print("no convergence in ", tmax, " steps, last r = ", r, "\n")
    (r, tmax, string("no convergence in ", tmax, " steps"))
end

"""
    validate_Hensel(l::Int, f::PolyElem, g::PolyElem, h::PolyElem)

Compute a numeric approximation of Hensel and then validate the results, returning interval valued polynomial results for g and h
"""
function validate_Hensel_a_posteriori(l::Int, f::PolyElem, g::PolyElem, h::PolyElem; ϵ = 1e-10, tmax = 20)
    #convert f,g,h to polynomials in Float64
    fr = convert_balls_to_Float(f)
    gr = convert_balls_to_Float(g)
    hr = convert_balls_to_Float(h)
    dg = degree(g)
    dh = degree(h)
    #compute numerical solution
    (gstar, hstar, s, t )= henseltruncate(l,fr,gr,hr,validation = false)
    @show degree(gstar)
    @show degree(hstar)
    @show gstar
    igstar = convert_float_to_balls(gstar)
    ihstar = convert_float_to_balls(hstar)
    s = convert_float_to_balls(s)
    t = convert_float_to_balls(t)
    #compute δ
    (q,R) = fastdivrem(s*(igstar*ihstar-f),ihstar, l, validation = true)

    δg = t*(igstar*ihstar-f) + q*igstar
    δg = mag_coefficientwise(δg)
    #BivPolMod!(δg,l)
    δg = trunc(δg, dg+1,l)
    #@show δg
    
    δh = mag_coefficientwise(R)
    #BivPolMod!(δh,l)
    δh = trunc(δh, dh,l)
    #@show δh
    δgmin = nonzeroMin(δg)
    
    ηg = ϵ*deepcopy(δg)
    replace_zeros!(ηg,ϵ*δgmin, dg+1, l)
    δhmin = nonzeroMin(δh)
    
    ηh = ϵ*deepcopy(δh)
    replace_zeros!(ηh, ϵ*δhmin, dh, l)
    
    δh = δh + ηh
    δg = δg + ηg
    @show degree(δg)
    @show degree(δh)

    invrevh = newtoninversion(rev(ihstar,degree(ihstar)+1),degree(ihstar)+1,l,validation = true)
    u = s*igstar + t*ihstar -1

    #validation
    rg = parent(g)(0)
    rh = parent(h)(0)
    for t in 1:tmax
        (Λg, Λh) = Λ(rg,rh,s,t,u, igstar, ihstar, invrevh)
        Λg = trunc(Λg, dg+1,l)
        Λh = trunc(Λh, dh, l)
        
        rrg = mag_coefficientwise(Λg*rg + δg)
        rrg = trunc(rrg, dg+1, l)
        @show degree(rrg)

        rrh = mag_coefficientwise(Λh*rh + δh)
        rrh = trunc(rrh, dh, l)
        @show degree(rrh)
        
        @show (isless_coefficientwise(rrg-rg,ηg))
        @show (isless_coefficientwise(rrh-rh,ηh))
        
        if (isless_coefficientwise(rrg-rg,ηg))&&(isless_coefficientwise(rrh-rh,ηh))
            gstar = convert_polynomial_to_intervals(gstar,convert_balls_to_Float(rg,rounding = RoundUp))
            hstar = convert_polynomial_to_intervals(hstar,convert_balls_to_Float(rh,rounding = RoundUp)) 
            return(gstar,hstar)       
        end  
        rg = rrg
        rh = rrh
    end
    print("no convergence after ", tmax, " steps\n")
    (rg,rh)
end

"""
    Lambda(rg,rh, s,t, ϵ, g, h, invrevh)

Helper function for the validation of Hensel.
# Arguments
- `rg,rh` current iteratives of the radii of g and handle
- `s,t,g,h` our initial numerical approximations
- `invrevh` rev(h)^-1 so it can be computed only once and reused in following iterations, validated 
- `ϵ` s*g+t*h = 1 + ϵ
"""
function Λ(rg,rh, s,t, ϵ, g, h, invrevh)
    rrg = convert_balls_to_Float(rg, rounding = RoundUp)
    rrh = convert_balls_to_Float(rh, rounding = RoundUp)
    irg = convert_polynomial_to_intervals(parent(rrg)(0),rrg)
    irh = convert_polynomial_to_intervals(parent(rrh)(0),rrh)
    #Λg
    a = (2*s*irg + 1 + ϵ)*irh
    reva = rev(a,degree(a)+1)
    divabyh = reva*invrevh
    Λg = 2*t*irg*irh + ϵ*irg + g*rev(divabyh, degree(divabyh)+1)
    Λg = mag_coefficientwise(-Λg)
    #Λh
    a2 = 2*s*irh*irg +ϵ*irh
    reva2 = rev(a2, degree(a2)+1)
    qstar = reva2*invrevh
    q = rev(qstar, degree(qstar)+1)
    Λh = a2 - h*q
    Λh = mag_coefficientwise(-Λh)
    (Λg, Λh)
end