using Nemo

"""
    abs_coefficientwise!(p::PolyElem{T}) where T
"""
function abs_coefficientwise!(p::PolyElem{T}) where T
    dp = degree(p)
    if T <: Union{FieldElem,AbstractFloat}
        for i ∈ 1:dp+1
            set_coefficient!(p,i-1,abs(coeff(p,i-1)))
        end
    elseif T <: PolyElem
        for i ∈ 1:dp+1
            abs_coefficientwise!(coeff(p,i-1))
        end
    end
    p
end

"""
    isless_coefficientwise(p::PolyElem{S},q::PolyElem{T}) where {S <: Union{FieldElem, AbstractFloat}, T <: Union{FieldElem, AbstractFloat}}

Compare two polynomials and return true if all p_i < q_i ∀ i.
"""
function isless_coefficientwise(p::PolyElem{S},q::PolyElem{T}) where {S <: Union{FieldElem, AbstractFloat}, T <: Union{FieldElem, AbstractFloat}}
    d = max(degree(p), degree(q))
    for i in 1:d+1
        #print("[",Float64(coeff(p,i-1))-Float64(radius(coeff(p,i-1))),", ", Float64(coeff(p,i-1))+Float64(radius(coeff(p,i-1))), "] < [", Float64(coeff(q,i-1)) -Float64(radius(coeff(q,i-1))),", ", Float64(coeff(q,i-1)) +Float64(radius(coeff(q,i-1))), "] ?\n")
        if ((coeff(p,i-1) < coeff(q, i-1)) == false)
            return false   
        end
    end
    return true
end

"""
    isless_coefficientwise(p::PolyElem{S},q::PolyElem{T}) where {S <: PolyElem, T <: PolyElem}

Return true if all coefficients of p are smaller than their corresponding coefficient of q.
Return false else.
"""
function isless_coefficientwise(p::PolyElem{S},q::PolyElem{T}) where {S <: PolyElem, T <: PolyElem}
    print("isless()\n")
    d = max(degree(p), degree(q))
    for i in 1:d+1
        di = max(degree(p.coeffs[i]),degree(p.coeffs[i]))
        for j in 1:di+1
            #print(coeff(coeff(p,i-1),j-1), " < ", coeff(coeff(q,i-1),j-1), "?\n")

            if ((coeff(coeff(p,i-1),j-1) < coeff(coeff(q,i-1),j-1)) == false)
                return false
            end
        end
    end
    return true
end

"""
    isless_coefficientwise(p::PolyElem{T},q::PolyElem{T}) where T <: arb_poly

Return true if all coefficients of p are smaller than their corresponding coefficient of q.
Return false else.
"""
function isless_coefficientwise(p::PolyElem{T},q::PolyElem{T}) where T <: arb_poly
    d = max(degree(p), degree(q))
    for i in 1:d+1
        di = max(degree(p.coeffs[i]),degree(p.coeffs[i]))
        for j in 1:di+1
            #print("[",Float64(coeff(coeff(p,i-1),j-1))-Float64(radius(coeff(coeff(p,i-1),j-1))),", ", Float64(coeff(coeff(p,i-1),j-1))+Float64(radius(coeff(coeff(p,i-1),j-1))), "] < [", Float64(coeff(coeff(q,i-1),j-1)) -Float64(radius(coeff(coeff(q,i-1),j-1))),", ", Float64(coeff(coeff(q,i-1),j-1)) +Float64(radius(coeff(coeff(q,i-1),j-1))), "] ?\n")
            
            if ((coeff(coeff(p,i-1),j-1) < coeff(coeff(q,i-1),j-1)) == false)
                @show (i-1)
                @show (j-1)
                @show (coeff(coeff(p,i-1),j-1))
                @show coeff(coeff(q,i-1),j-1)
                return false
            end
        end
    end
    return true
end

"""
    nonzeroMin(d::arb_poly)

Return the smallest nonzero coefficient of polynomial d.
"""
function nonzeroMin(d::arb_poly) 
    RR = ArbField(53)
    min = RR(0)
    pd = degree(d)
    for i in 1:pd+1
        if ((coeff(d,i-1) < min)&&(coeff(d,i-1) != 0))
            min = coeff(d,i-1)
        elseif (min == 0)
            min = coeff(d,i-1)       
        end        
    end
    min
end

"""
    nonzeroMin(d::PolyElem{T}) where T <: PolyRingElem

Return the smallest nonzero coefficient of polynomial d.
"""
function nonzeroMin(d::PolyElem{T}) where T <: PolyElem
    #min = coeff(d, 0)
    min = coeff(coeff(d,0),0)
    pd = degree(d)
    for i in 1:pd+1
        min_i = nonzeroMin(coeff(d,i-1))
        if (min == 0)||((min_i < min)&&(min_i != 0))
            min = min_i         
        end        
    end
    min
end

"""
    nonzeroMin(d::PolyElem{T}) where T <: Union{RingElem, AbstractFloat}

Return the smallest nonzero coefficient of polynomial d.
"""
function nonzeroMin(d::PolyElem{T}) where T <: Union{FieldElem, AbstractFloat}
    min = coeff(d, 0) 
    pd = degree(d)
    for i in 2:pd+1
        if (min == 0)||((coeff(d,i-1) < min)&&(coeff(d,i-1) != 0))
            min = coeff(d,i-1)    
        end        
    end
    min
end

"""
    range_of_coefficients(p::PolyElem{T}) where T

Return the smallest (nonzero) and highest absolute value of the coefficients of p
"""
function range_of_coefficients(p::PolyElem{T}) where T
    dp = degree(p)
    min = 0.0
    max = 0.0
    q = deepcopy(p)
    abs_coefficientwise!(q)
    for i in 0:dp
        if T <: PolyElem
            (mini, maxi) = range_of_coefficients(coeff(q,i))
        else
            mini = coeff(q,i)
            maxi = coeff(q,i)
        end  
        if (mini != 0)&&(mini != 1) && ((min == 0)||((min != 0)&&(mini < min)))
            min = mini                
        end
        if (maxi != 0)&&(maxi != 1) && ((max == 0)||((max != 0)&&(maxi > max)))
            max = maxi                
        end        
    end   
    (min,max)
end

"""
    convert_float_to_balls(p::PolyElem{T}) where T <: PolyElem

Convert a real polynomial p to a polynomial with thin intervals around the coefficients of p.
"""
function convert_float_to_balls(p::PolyElem{T}) where T <: PolyElem
    dp = degree(p)
    RR = ArbField(53)
    RBX, _ = PolynomialRing(RR, "x")
    RBXY, _ = PolynomialRing(RBX, "y")
    q =  RBXY(0.0)
    for i ∈ 1:dp+1  
        #@show coeff(p,i-1)
        set_coefficient!(q,i-1,convert_float_to_balls(coeff(p,i-1)))
    end
    q
end

"""
    convert_float_to_balls(p::PolyElem{T})  where T<:Union{AbstractFloat, FieldElem}

Convert a real polynomial p to a polynomial with thin intervals around the coefficients of p.
"""
function convert_float_to_balls(p::PolyElem{T})  where T<:Union{AbstractFloat, FieldElem}
    dp = degree(p)
    RR = ArbField(53)
    RBX, _ = PolynomialRing(RR, "x")
    q = RBX(0.0)
    for i ∈ 1:dp+1
        #@show coeff(p,i-1)
        #@show RR(coeff(p,i-1))
        set_coefficient!(q,i-1, RR(coeff(p,i-1)))
    end
    q
end

"""
    convert_balls_to_Float(p::PolyElem{T}; rounding = RoundNearest) where T

Convert a polynomial p with interval coefficients into a polynomial with real coefficients. By definition the coefficients will be the interval midpoints, but with changes to the rounding methods, it can also return the upper bound or lower bound values of p.
"""
function convert_balls_to_Float(p::PolyElem{T}; rounding = RoundNearest) where T
    dp = degree(p)
    RX, _ = PolynomialRing(RDF, "x")
    q = 0
    if T <: FieldElem
        q = RX(0.0)
        for i ∈ 1:dp+1
            set_coefficient!(q,i-1, Float64(coeff(p,i-1), rounding))
        end
    elseif T <: PolyElem
        RXY, _ = PolynomialRing(RX, "y")
        q = RXY(0.0)
        for i ∈ 1:dp+1
            set_coefficient!(q,i-1, convert_balls_to_Float(coeff(p,i-1), rounding = rounding))
        end
    end
    q
end

function convert_balls_to_upperBound_Float(p::PolyElem{T}) where T
    convert_balls_to_Float(p,rounding = RoundUp)
end

function convert_balls_to_midpoint_Float(p::PolyElem{T}) where T
    convert_balls_to_Float(p)
end

function convert_balls_to_lowerBound_Float(p::PolyElem{T}) where T
    convert_balls_to_Float(p,rounding = RoundDown)
end

"""
    mag(a::T) where T <: Union{arb,acb}

Return max(abs(a))
"""
function mag(a::T) where T <: Union{arb,acb}
    m = abs(a)
    m = RR(Float64(m, RoundUp))
end

"""
    mag_coefficientwise(p::PolyElem{T}) where T

Apply max(abs(*)) on each coefficient of p.
"""
function mag_coefficientwise(p::PolyElem{T}) where T
    dp = degree(p)
    RR = ArbField(53)
    RBX, _ = PolynomialRing(RR, "x")
    if T <: arb
        q = RBX(0)
        for i ∈ 1:dp+1
            set_coefficient!(q,i-1,mag(coeff(p,i-1)))
            #set_coefficient!(q,i-1, RR(Nemo._arb_get_arf(coeff(p,i-1), RoundUp))))
        end
    elseif T <: arb_poly
        RBXY, _ = PolynomialRing(RBX, "y")
        q = RBXY(0)
        for i ∈ 1:dp+1
            set_coefficient!(q, i-1, mag_coefficientwise(coeff(p,i-1)))
        end
    end
    q
end

function printpoly(p::PolyElem)
    @show convert_balls_to_lowerBound_Float(p)
    @show convert_balls_to_upperBound_Float(p)
end

"""
    comparepoly(p::PolyElem{T}, q::PolyElem{T}) where T <:Union{arb,arb_poly}

Prints indexes and corresponding coefficients of p and q in a line each.
"""
function comparepoly(p::PolyElem{T}, q::PolyElem{T}) where T <:Union{arb,arb_poly}
    d = max(degree(p), degree(q))
    if T <: PolyElem
        for i in 1:d+1
            print("j = ", i, "\n")
            comparepoly(coeff(p,i-1), coeff(q,i-1))
        end
    else
        for i in 1:d+1
           #print("i = ", i-1, ":\t midpoints: \t", Float64(midpoint(coeff(p,i-1))), "\t", Float64(midpoint(coeff(q,i-1))),"\n")
           #print("\t radii: \t", Float64(radius(coeff(p,i-1))), "\t", Float64(radius(coeff(q,i-1))),"\n") 
           print("i = ", i-1, ":\t midpoints: \t", midpoint(coeff(p,i-1)), "\t", midpoint(coeff(q,i-1)),"\n")
           print("\t radii: \t", radius(coeff(p,i-1)), "\t", radius(coeff(q,i-1)),"\n")
           @show (coeff(p,i-1)==coeff(q,i-1))
        end
    end
end

"""
    comparepoly(p::PolyElem{T}, q::PolyElem{S}) where {T,S}

Prints indexes and corresponding coefficients of p and q in a line each.
"""
function comparepoly(p::PolyElem{T}, q::PolyElem{S}) where {T,S}
    d = max(degree(p), degree(q))
    if T <: PolyElem
        for i in 1:d+1
            print("j = ", i, "\n")
            comparepoly(coeff(p,i-1), coeff(q,i-1))
        end
    else
        for i in 1:d+1
           print("i = ", i-1, "\t", coeff(p,i-1), "\t", coeff(q,i-1),"\n")
           #@show (coeff(p,i-1)<coeff(q,i-1))
        end
    end
end

function sub(q,p)
    print("\n")
    for i ∈ 1:degree(q)+1
        print("i = ", i, ": ", coeff(q,i-1), " - ", coeff(p,i-1), " = ", coeff(q,i-1) -  coeff(p,i-1), "\n")    
    end
end

"""
    subs(q::PolyElem{T},p::PolyElem) where {T<:Union{PolyElem,AbstractFloat}}

Subtract a polynomial p with rational coefficients from a polynomial q with real coefficients.
"""
function subs(q::PolyElem{T},p::PolyElem) where {T<:Union{PolyElem,AbstractFloat}}
    d = max(degree(q),degree(p))
    FB = Nemo.AbstractAlgebra.Floats{BigFloat}()
    BFX, x_bf = PolynomialRing(FB, "x")
    if T <: AbstractFloat
        r = BFX(0)
        for i ∈ 0:d
            set_coefficient!(r,i,abs(coeff(q, i)-convert(Rational{BigInt}, coeff(p, i))))
            print("q \t", coeff(q,i), "\t p \t", coeff(p,i), "\t r \t", coeff(r,i), "\n")
        end
    else
        BFXY, y_bf = PolynomialRing(BFX, "y")
        r = BFXY(0)
        for i ∈ 0:d
            set_coefficient!(r,i,subs(coeff(q,i),coeff(p,i)))
        end
    end
    r
end

"""
    convert_polynomial(p::PolyElem{T},listOfRings, coeff_field) where {T <: Union{QQPolyRingElem,QQFieldElem}}

Convert a polynomial with rational coefficients to a polynomial with coefficients in coeff_field.
listOfRings includes an array of polynomial rings the polynomial should be converted to, starting with the outermost ring, followed by the polynomial ring of its coefficients, etc.
"""
function convert_polynomial(p::PolyElem{T},listOfRings, coeff_field) where {T <: Union{QQPolyRingElem,QQFieldElem}}
    d = degree(p)
    q = (listOfRings[1])(0)
    if T <: PolyElem
        for i ∈ 0:d
            set_coefficient!(q,i,convert_polynomial(coeff(p,i), listOfRings[2:length(listOfRings)], coeff_field))
        end
    elseif typeof(coeff_field) <: Union{FieldElem,ArbField}
        for i ∈ 0:d
            set_coefficient!(q,i,coeff_field(coeff(p,i)))            
        end
    else
        for i ∈ 0:d
            set_coefficient!(q,i, coeff_field(convert(Rational{BigInt},coeff(p,i))))
        end
    end
    q
end

"""
    convert_polynomial_to_intervals(p::PolyElem{T},r::PolyElem{T}) where T

Return a polynomial with interval valued coefficients, whose coefficients are intervals around the coefficients of p with the radius being the coefficients of r.
"""
function convert_polynomial_to_intervals(p::PolyElem{T},r::PolyElem{T}) where T
    d = max(degree(p),degree(r))
    RR = ArbField(53)
    RBX, _ = PolynomialRing(RR, "x")
    if T <: PolyElem
        RBXY, _ = PolynomialRing(RBX, "y")
        pi = RBXY(0)
        for i in 0:d
            set_coefficient!(pi,i,convert_polynomial_to_intervals(coeff(p,i),coeff(r,i)))            
        end        
    else
        pi = RBX(0)
        for i in 0:d
            #@show i
            #print(coeff(p,i), "+/-", coeff(r,i),"\n")
            s = string(coeff(p,i),"+/-",coeff(r,i))
            set_coefficient!(pi,i,RR(s))
            #print(midpoint(coeff(pi,i)), "+/-", radius(coeff(pi,i)),"\n") 
        end
    end
    pi
end

"""
    extract_radius(p::PolyElem{T}) where T

Return a polynomial whose coefficients are the radii of the coefficients of the input polynomial p.
"""
function extract_radius(p::PolyElem{T}) where T
    r = parent(p)(0)
    d = degree(p)
    for i in 0:d
        if T <: PolyElem
            set_coefficient!(r,i,extract_radius(coeff(p,i)))
        else
            set_coefficient!(r,i,mag(radius(coeff(p,i))))            
        end        
    end
    r
end

"""
    replace_zeros!(p::PolyElem{T}, d, dp = -1, k = -1) where T

Replaces zero coefficients in polynomial p by the value d, dp being the wanted degree of the output polynomial, and k the degree of the coefficients for bivariate input polynomials.
"""
function replace_zeros!(p::PolyElem{T}, d, dp = -1, k = -1) where T
    if (T <: PolyElem)&&(dp == -1)
        deg = degree(p)
    elseif (T <: PolyElem)
        deg = dp
    else
        deg = k        
    end
    @show (deg-1)
    for i in 0:deg-1
        if T <: PolyElem
            set_coefficient!(p,i,replace_zeros!(coeff(p,i),d,dp,k))
        elseif coeff(p,i) == 0
            set_coefficient!(p,i,d)
        end
    end    
    p
end

"""
    trunc(p::PolyElem, k::Int, l::Int)

Truncate p in mod y^k mod x^l
"""
function trunc(p::PolyElem, k::Int, l::Int)
    q = truncate(p,k)
    BivPolMod!(q,l)
    q    
end